<?php
require_once 'ConnexionBaseDeDonnees.php'; // Inclusion de la connexion à la BDD
require_once 'Utilisateur.php'; // Inclusion de la classe Utilisateur
require_once 'Trajet.php'; // Inclusion de la classe Trajet

// Vérifier si le formulaire a été soumis
if (!empty($_POST)) {

    // Récupérer les données du formulaire
    $depart = $_POST['depart'] ?? '';
    $arrivee = $_POST['arrivee'] ?? '';
    $dateString = $_POST['date'] ?? '';  // La date est une string dans le formulaire
    $prix = $_POST['prix'] ?? 0;
    $conducteurLogin = $_POST['conducteurLogin'] ?? '';
    $nonFumeur = isset($_POST['nonFumeur']) ? true : false;

    // Vérifier que toutes les informations nécessaires sont présentes
    if ($depart && $arrivee && $dateString && $prix && $conducteurLogin) {
        // Convertir la date string en DateTime
        $date = new DateTime($dateString);

        // Récupérer l'utilisateur conducteur via son login
        $conducteur = Utilisateur::recupererUtilisateurParLogin($conducteurLogin);

        // Créer l'objet Trajet avec l'ID à null
        $trajet = new Trajet(
            null,           // ID inconnu, donc null
            $depart,        // Lieu de départ
            $arrivee,       // Lieu d'arrivée
            $date,          // Date du trajet (convertie en DateTime)
            $prix,          // Prix du trajet
            $conducteur,    // Conducteur (objet Utilisateur)
            $nonFumeur      // Statut non-fumeur
        );

        // Appeler la méthode pour ajouter le trajet dans la base de données
        $trajet->ajouter();

        // Confirmation de la création du trajet
        echo "<p>Le trajet de $depart à $arrivee pour {$prix} euros a été créé avec succès.</p>";
        var_dump($trajet);
    } else {
        echo "<p>Veuillez remplir tous les champs du formulaire.</p>";
    }
}
