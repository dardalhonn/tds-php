<?php

use Modele\ConnexionBaseDeDonnees;

require_once 'ConnexionBaseDeDonnees.php';
require_once 'Utilisateur.php';

class Trajet {

    private ?int $id;

    private array $passagers;
    private string $depart;
    private string $arrivee;
    private DateTime $date;
    private int $prix;
    private bool $nonFumeur;
    /**
     * @var Utilisateur[]
     */
    private Utilisateur $conducteur;


    public function __construct(
        ?int $id,
        string $depart,
        string $arrivee,
        DateTime $date,
        int $prix,
        Utilisateur $conducteur,
        bool $nonFumeur,
        array $passagers = []
    )
    {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrivee = $arrivee;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteur = $conducteur;
        $this->nonFumeur = $nonFumeur;
        $this->passagers = $passagers;
    }

    public static function construireDepuisTableauSQL(array $trajetTableau): Trajet {
        // Créer un trajet avec une liste vide de passagers
        $trajet = new Trajet(
            $trajetTableau['id'],
            $trajetTableau['depart'],
            $trajetTableau['arrive'],
            new DateTime($trajetTableau['date']),
            $trajetTableau['prix'],
            Utilisateur::recupererUtilisateurParLogin($trajetTableau['conducteurLogin']),
            (bool) $trajetTableau['nonFumeur'],

        );

        // Récupérer les passagers pour ce trajet
        $trajet->passagers = $trajet->recupererPassagers();

        return $trajet;
    }


    public function getId(): int
    {
        return $this->id;
    }
    public function getPassagers(): array {
        return $this->passagers;
    }

    public function setPassagers(array $passagers): void {
        $this->passagers = $passagers;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getDepart(): string
    {
        return $this->depart;
    }

    public function setDepart(string $depart): void
    {
        $this->depart = $depart;
    }

    public function getArrivee(): string
    {
        return $this->arrivee;
    }

    public function setArrivee(string $arrivee): void
    {
        $this->arrivee = $arrivee;
    }

    public function getDate(): DateTime
    {
        return $this->date;
    }

    public function setDate(DateTime $date): void
    {
        $this->date = $date;
    }

    public function getPrix(): int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): void
    {
        $this->prix = $prix;
    }

    public function getConducteur(): Utilisateur
    {
        return $this->conducteur;
    }

    public function setConducteur(Utilisateur $conducteur): void
    {
        $this->conducteur = $conducteur;
    }

    public function isNonFumeur(): bool
    {
        return $this->nonFumeur;
    }

    public function setNonFumeur(bool $nonFumeur): void
    {
        $this->nonFumeur = $nonFumeur;
    }

    public function __toString()
    {
        $nonFumeur = $this->nonFumeur ? " non fumeur" : " ";
        return "<p>
            Le trajet$nonFumeur du {$this->date->format("d/m/Y")} partira de {$this->depart} pour aller à {$this->arrivee} (conducteur: {$this->conducteur->getPrenom()} {$this->conducteur->getNom()}).
        </p>";
    }

    /**
     * @return Trajet[]
     */
    public static function recupererTrajets() : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPDO()->query("SELECT * FROM trajet");

        $trajets = [];
        foreach($pdoStatement as $trajetFormatTableau) {
            $trajets[] = Trajet::construireDepuisTableauSQL($trajetFormatTableau);
        }

        return $trajets;
    }

    public function ajouter() {
        // Préparer la requête d'insertion
        $pdo = ConnexionBaseDeDonnees::getPDO();
        $requete = $pdo->prepare("
            INSERT INTO trajet (id,depart, arrive, date, prix, conducteurLogin, nonFumeur)
            VALUES (:id,:depart, :arrive, :date, :prix, :conducteurLogin, :nonFumeur)
        ");

        // Exécuter la requête avec les valeurs préparées
        $requete->execute([
            'id' => $this->id,
            'depart' => $this->depart,
            'arrive' => $this->arrivee,
            'date' => $this->date->format('Y-m-d'),  // Formatage de la date pour MySQL
            'prix' => $this->prix,
            'conducteurLogin' => $this->conducteur->getLogin(), // On n'insère que le login
            'nonFumeur' => $this->nonFumeur ? 1 : 0 // Conversion du booléen en entier (1 ou 0)
        ]);
    }
    public function recupererPassagers() : array {
        // Connexion à la base de données
        $pdo = ConnexionBaseDeDonnees::getPDO();

        // Préparer la requête SQL avec INNER JOIN
        $requete = $pdo->prepare("
            SELECT u.*
            FROM utilisateur u
            INNER JOIN passager p ON u.login = p.passagerLogin
            WHERE p.trajetId = :trajetId
        ");

        // Exécuter la requête avec l'ID du trajet actuel
        $requete->execute(['trajetId' => $this->id]);

        // Récupérer les résultats sous forme de tableau associatif
        $passagersTableau = $requete->fetchAll(PDO::FETCH_ASSOC);

        // Créer des objets Utilisateur à partir des résultats
        $passagers = [];
        foreach ($passagersTableau as $passager) {
            // Supposons que la méthode Utilisateur::construireDepuisTableauSQL existe
            $passagers[] = Utilisateur::construireDepuisTableauSQL($passager);
        }

        return $passagers;
    }
}
