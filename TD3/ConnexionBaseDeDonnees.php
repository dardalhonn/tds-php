<?php


use Configuration\ConfigurationBaseDeDonnees;

require_once 'ConfigurationBaseDeDonnees.php';

class ConnexionBaseDeDonnees
{

    private static ?Modele\ConnexionBaseDeDonnees $instance = null;
    private PDO $PDO;


    public function __construct()
    {
        $nomHote = ConfigurationBaseDeDonnees::getNomHote();
        $port = ConfigurationBaseDeDonnees::getPort();
        $nomBaseDeDonnees = ConfigurationBaseDeDonnees::getNomBaseDeDonnees();
        $login = ConfigurationBaseDeDonnees::getLogin();
        $motDePasse = ConfigurationBaseDeDonnees::getPassWord();

        $this->PDO = new PDO("mysql:host=$nomHote;port=$port;dbname=$nomBaseDeDonnees", $login, $motDePasse,
            array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));

// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
        $this->PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

    }

    private static function getInstance(): Modele\ConnexionBaseDeDonnees
    {
        // L'attribut statique $instance s'obtient avec la syntaxe ConnexionBaseDeDonnees::$instance
        if (is_null(Modele\ConnexionBaseDeDonnees::$instance))
            // Appel du constructeur

            Modele\ConnexionBaseDeDonnees::$instance = new Modele\ConnexionBaseDeDonnees();

        return Modele\ConnexionBaseDeDonnees::$instance;
    }

    public static function getPdo(): PDO
    {

        return Modele\ConnexionBaseDeDonnees::getInstance()->PDO;
    }


}

