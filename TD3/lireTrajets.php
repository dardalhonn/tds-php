<?php
require_once 'ConnexionBaseDeDonnees.php';
require_once 'Trajet.php';

// Récupérer tous les trajets
$trajets = Trajet::recupererTrajets();

// Afficher chaque trajet
foreach ($trajets as $trajet) {
    echo $trajet;
}
