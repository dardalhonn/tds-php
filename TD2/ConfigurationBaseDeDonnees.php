<?php
class ConfigurationBaseDeDonnees {

    static private array $configurationBaseDeDonnees = array(
        // Le nom d'hote est webinfo a l'IUT
        // ou localhost sur votre machine
        //
        // ou webinfo.iutmontp.univ-montp2.fr
        // pour accéder à webinfo depuis l'extérieur
        'nomHote' => 'webinfo.iutmontp.univ-montp2.fr',
        // A l'IUT, vous avez une base de données nommee comme votre login
        // Sur votre machine, vous devrez creer une base de données
        'nomBaseDeDonnees' => 'dardalhonn',
        // À l'IUT, le port de MySQL est particulier : 3316
        // Ailleurs, on utilise le port par défaut : 3306
        'port' => '3316',
        // A l'IUT, c'est votre login
        // Sur votre machine, vous avez surement un compte 'root'
        'login' => 'dardalhonn',
        // A l'IUT, c'est le même mdp que PhpMyAdmin
        // Sur votre machine personelle, vous avez creez ce mdp a l'installation
        'motDePasse' => 'Gvbld7c3'
    );

    static function getNomHote(): string{
        return self::$configurationBaseDeDonnees['nomHote'];
    }

    static function getNomBaseDeDonnees(): string{
        return self::$configurationBaseDeDonnees['nomBaseDeDonnees'];
    }

    static function getPort():string
    {
        return self::$configurationBaseDeDonnees['port'];
    }

    static function getPassWord(): string{
        return self::$configurationBaseDeDonnees['motDePasse'];
    }
    static public function getLogin() : string {
        // L'attribut statique $configurationBaseDeDonnees
        // s'obtient avec la syntaxe ConfigurationBaseDeDonnees::$configurationBaseDeDonnees
        // au lieu de $this->configurationBaseDeDonnees pour un attribut non statique
        return self::$configurationBaseDeDonnees['login'];
    }

}
?>

