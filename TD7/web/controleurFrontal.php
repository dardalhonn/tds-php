<?php
require_once __dir__ . "./../src/Lib/Psr4AutoloaderClass.php";
// On récupère l'action passée dans l'URL
if (isset($_GET['action']))
    $action = $_GET['action'];
else
    $action = "afficherListe";


$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

use App\Covoiturage\Lib\PreferenceControleur;
if (isset($_GET['controleur']))
    $controleur = $_GET['controleur'];
else
    $controleur = PreferenceControleur::lire() == ""? "utilisateur" : PreferenceControleur::lire();

$nomDeClasseControleur = "App\Covoiturage\Controleur\Controleur".ucfirst($controleur);

use App\Covoiturage\Controleur\ControleurUtilisateur;
if(!class_exists($nomDeClasseControleur)) {
    ControleurUtilisateur::afficherErreur("La méthode $action n'existe pas");
}


// Appel de la méthode statique $action de ControleurUtilisateur
$array = get_class_methods($nomDeClasseControleur);
if(in_array($action, $array))
    // on viens de verifier que une methode de nom $action existe dans ControleurUtilisateur
    $nomDeClasseControleur::$action();
else
    $nomDeClasseControleur::afficherErreur("La méthode $action n'existe pas");
?>
