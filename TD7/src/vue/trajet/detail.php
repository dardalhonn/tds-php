<?php
use \App\Covoiturage\Modele\DataObject\Trajet;
/** @var Trajet $parametres["trajet"] */
$trajet = $parametres["trajet"];
/** @var  $nonFumeur */
$nonFumeur = $trajet->isNonFumeur() ? " non fumeur" : " ";
$date = $trajet->getDate()->format("d/m/Y");

$depart = htmlspecialchars($trajet->getDepart());
$arrivee = htmlspecialchars($trajet->getArrivee());

$nomConducteur = htmlspecialchars($trajet->getConducteur()->getNom());
$prenomConducteur = htmlspecialchars($trajet->getConducteur()->getPrenom());

echo "Le trajet$nonFumeur du $date partira de $depart pour aller à $arrivee 
(conducteur: $nomConducteur $prenomConducteur).
";

?>
