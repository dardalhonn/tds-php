<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>
<body>
<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='controleur' value='trajet'>
    <input type='hidden' name='action' value='mettreAJour'>
    <input type='hidden' name='id' value=<?=$parametres["trajet"]->getId() ?>>
    <fieldset>
        <legend>formulaire Modification Trajet :</legend>
        <p class="InputAddOn">
            <label for="login_id" class="InputAddOn-item">Ville de départ</label> :
            <input class="InputAddOn-field"  type="text" value=<?= htmlspecialchars($parametres["trajet"]->getDepart()) ?>
            name="depart" id="depart_id" required/>
        </p>
        <p class="InputAddOn">
            <label for="arriver_id" class="InputAddOn-item">ville d'arrivee</label> :
            <input class="InputAddOn-field"  type="text" value=<?= htmlspecialchars($parametres["trajet"]->getArrivee()) ?>
            name="arriver" id="arriver_id" required />
        </p>
        <p class="InputAddOn">
            <label for="date_id" class="InputAddOn-item">Date</label> :
            <input class="InputAddOn-field" type="date" value=<?= htmlspecialchars($parametres["trajet"]->getDate()->format("Y-m-d")) ?>
            name="date" id=date_id" required />
        </p>
        <p class="InputAddOn">
            <label for="prix_id" class="InputAddOn-item">Prix d'une Place</label> :
            <input class="InputAddOn-field"  type="number" value=<?= htmlspecialchars($parametres["trajet"]->getPrix()) ?>
            name="prix" id="prix_id" min="0" required />
        </p>
        <p class="InputAddOn">
            <label for="login_id" class="InputAddOn-item">Login du Conducteur</label> :
            <input class="InputAddOn-field"  type="text" value=<?= htmlspecialchars($parametres["trajet"]->getConducteur()->getLogin()) ?>
            name="login" id="login_id" required />
        </p>
        <p class="InputAddOn">
            <label for="nonFumeur_id" class="InputAddOn-item">Voyage Non Fumeur</label> :
            <input class="InputAddOn-field"  type="checkbox" name="nonFumeur" id="nonFumeur_id"
                <?=$parametres["trajet"]->isNonFumeur()?"checked":"" ?>/>
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>
</html>
