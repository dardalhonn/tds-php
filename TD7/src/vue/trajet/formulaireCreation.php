<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8" />
    <title> Mon premier php </title>
</head>
<body>
<form method="get" action="controleurFrontal.php">
    <input type='hidden' name='controleur' value='trajet'>
    <input type='hidden' name='action' value='creerDepuisFormulaire'>
    <fieldset>
        <legend>formulaire Creation Trajet :</legend>
        <p class="InputAddOn">
            <label for="login_id" class="InputAddOn-item">Ville de départ</label> :
            <input class="InputAddOn-field"  type="text" placeholder="ville1" name="depart" id="depart_id" required/>
        </p>
        <p class="InputAddOn">
            <label for="arriver_id" class="InputAddOn-item">ville d'arrivee</label> :
            <input class="InputAddOn-field"  type="text" placeholder="Ville2" name="arriver" id="arriver_id" required />
        </p>
        <p class="InputAddOn">
            <label for="date_id" class="InputAddOn-item">Date</label> :
            <input class="InputAddOn-field" type="date" name="date" id=date_id" required />
        </p>
        <p class="InputAddOn">
            <label for="prix_id" class="InputAddOn-item">Prix d'une Place</label> :
            <input class="InputAddOn-field"  type="number" placeholder="100" name="prix" id="prix_id" min="0" required />
        </p>
        <p class="InputAddOn">
            <label for="login_id" class="InputAddOn-item">Login du Conducteur</label> :
            <input class="InputAddOn-field"  type="text" placeholder="benhalimam" name="login" id="login_id" required />
        </p>
        <p class="InputAddOn">
            <label for="nonFumeur_id" class="InputAddOn-item">Voyage Non Fumeur</label> :
            <input class="InputAddOn-field"  type="checkbox" name="nonFumeur" id="nonFumeur_id" />
        </p>
        <p>
            <input type="submit" value="Envoyer" />
        </p>
    </fieldset>
</form>
</body>
</html>
