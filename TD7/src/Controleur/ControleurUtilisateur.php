<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use App\Covoiturage\Modele\HTTP\Cookie;
class ControleurUtilisateur extends ControleurGenerique{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        $values = [
            "utilisateurs" => $utilisateurs,
            "titre" => "Liste des utilisateurs",
            "cheminCorpsVue" => "utilisateur/liste.php"
        ];
        self::afficherVue('vueGenerale.php',$values);
    }

    public static function afficherDetail() : void{
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        if($utilisateur != null) {
            $values = [
                "utilisateur" => $utilisateur,
                "titre" => "Detail Utilisateur",
                "cheminCorpsVue" => "utilisateur/detail.php"
            ];
            self::afficherVue("vueGenerale.php",$values);
        }
        else{
            self::afficherErreur("Il n'existe aucun utilisateur de login $login");
        }
    }

    public static function afficherFormulaireCreation() : void{
        $values = [
            "titre" => "Formulaire Création",
            "cheminCorpsVue" => 'utilisateur/formulaireUtilisateur.php'
        ];
        self::afficherVue("vueGenerale.php",$values);
    }

    public static function creerDepuisFormulaire() : void{
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        $values = [
            "titre" => "Formulaire Création",
            "cheminCorpsVue" => 'utilisateur/utilisateurCree.php',
            "utilisateurs" => $utilisateurs
        ];
        self::afficherVue('vueGenerale.php',$values);
    }

    public static function supprimer() : void{
        $login = $_GET['login'];
        (new UtilisateurRepository())->supprimer($login);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        $values = [
            "login" => "$login",
            "utilisateurs" => $utilisateurs,
            "titre" => "Votre utilisateur a été supprimé",
            "cheminCorpsVue" => 'utilisateur/utilisateurSupprime.php'
        ];
        self::afficherVue("vueGenerale.php",$values);
    }

    public static function afficherFormulaireMiseAJour(){
        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);
        $values = [
            "utilisateur" => $utilisateur,
            "titre" => "Formulaire Mise A Jour",
            "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php"
        ];
        self::afficherVue("vueGenerale.php",$values);
    }

    public static function mettreAJour() : void{
        $utilisateur = self::construireDepuisFormulaire($_GET);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        $values = [
            "titre" => "utilisateurMisAJour",
            "cheminCorpsVue" => 'utilisateur/utilisateurMisAJour.php',
            "utilisateurs" => $utilisateurs,
            "utilisateur" => $utilisateur
        ];
        self::afficherVue('vueGenerale.php',$values);
    }

    public static function afficherErreur(String $messageErreur = "") : void{
        $values = [
            "titre" => "Erreur lors de la navigation",
            "cheminCorpsVue" => 'utilisateur/erreur.php',
            "messageErreur" => $messageErreur
        ];
        self::afficherVue('vueGenerale.php',$values);
    }



    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];
        $utilisateur = new Utilisateur($login, $nom, $prenom);
        return $utilisateur;
    }

    public static function deposerCookie(){
        Cookie::enregistrer("cookie1","je me regale");
    }


    public static function lireCookie(){
        echo Cookie::lire("cookie1");
    }


}
?>
