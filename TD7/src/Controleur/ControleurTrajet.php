<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\TrajetRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;
use DateTime;

class ControleurTrajet extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $trajets = (new  TrajetRepository())->recuperer();
        $values = [
            "trajets" => $trajets,
            "titre" => "Liste des trajets",
            "cheminCorpsVue" => "trajet/liste.php"
        ];
        self::afficherVue('vueGenerale.php', $values);
    }

    public static function afficherDetail(): void
    {
        $id = $_GET['id'];
        $trajet =  (new  TrajetRepository())->recupererParClePrimaire($id);
        if ($trajet != null) {
            $values = [
                "trajet" => $trajet,
                "titre" => "Detail Trajet",
                "cheminCorpsVue" => "trajet/detail.php"
            ];
            self::afficherVue("vueGenerale.php", $values);
        } else {
            self::afficherErreur("Il n'existe aucun trajet d'id $id");
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        $values = [
            "titre" => "Formulaire Création de Trajet",
            "cheminCorpsVue" => 'trajet/formulaireCreation.php'
        ];
        self::afficherVue("vueGenerale.php", $values);
    }

    public static function creerDepuisFormulaire(): void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new  TrajetRepository())->ajouter($trajet);
        $trajets = (new  TrajetRepository())->recuperer();
        $values = [
            "trajets" => $trajets,
            "titre" => "Formulaire Création d'un Trajet",
            "cheminCorpsVue" => 'trajet/trajetCree.php',
        ];
        self::afficherVue('vueGenerale.php', $values);
    }

    public static function supprimer(): void
    {
        $id = $_GET['id'];
        (new TrajetRepository())->supprimer($id);
        $trajets = (new  TrajetRepository())->recuperer();
        $values = [
            "id" => $id,
            "trajets" => $trajets,
            "titre" => "Votre trajet a été supprimé",
            "cheminCorpsVue" => 'trajet/trajetSupprimer.php'
        ];
        self::afficherVue("vueGenerale.php", $values);
    }

    public static function afficherFormulaireMiseAJour()
    {
        $id = $_GET['id'];
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);
        $values = [
            "trajet" => $trajet,
            "titre" => "Formulaire Mise A Jour",
            "cheminCorpsVue" => "trajet/formulaireMiseAJour.php"
        ];
        self::afficherVue("vueGenerale.php", $values);
    }

    public static function mettreAJour(): void
    {
        $trajet = self::construireDepuisFormulaire($_GET);
        (new TrajetRepository())->mettreAJour($trajet);
        $trajets = (new  TrajetRepository())->recuperer();
        $values = [
            "titre" => "Trajet Mis à jour",
            "cheminCorpsVue" => 'trajet/trajetMisAJour.php',
            "trajets" => $trajets,
            "id" => $trajet->getId()
        ];
        self::afficherVue('vueGenerale.php', $values);
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        $values = [
            "titre" => "Erreur lors de la navigation",
            "cheminCorpsVue" => 'utilisateur/erreur.php',
            "messageErreur" => $messageErreur
        ];
        self::afficherVue('vueGenerale.php', $values);
    }

    public static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Trajet
    {
        $id = $tableauDonneesFormulaire["id"] ?? null;
        $depart = $tableauDonneesFormulaire['depart'];
        $arriver = $tableauDonneesFormulaire['arriver'];
        $date = new DateTime($tableauDonneesFormulaire["date"]);
        $prix = $tableauDonneesFormulaire['prix'];
        $login = $tableauDonneesFormulaire['login'];
        $nonFumeur = isset($tableauDonneesFormulaire['nonFumeur']);

        $conducteur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        $trajet = new Trajet($id, $depart, $arriver, $date, $prix, $conducteur, $nonFumeur);
        return $trajet;
    }

}