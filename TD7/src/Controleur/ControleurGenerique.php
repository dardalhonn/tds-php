<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\PreferenceControleur;

class ControleurGenerique{

    protected static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__."/../vue/$cheminVue"; // Charge la vue
    }

    public static function afficherFormulairePreference() : void {
        self::afficherVue("vueGenerale.php",["titre" => "formulairePreference",
                                                        "cheminCorpsVue" => "formulairePreference.php"]);
    }

    public static function enregistrerPreference() : void {
        $valeur = $_GET["controleur_defaut"];
        PreferenceControleur::enregistrer($valeur);
        self::afficherVue("vueGenerale.php",["titre" => "changement appliquer",
            "cheminCorpsVue" => "preferenceEnregistree.php"]);
    }
}