<?php

namespace App\Covoiturage\Modele\Repository;


use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;

class UtilisateurRepository extends AbstractRepository
{



    public function construireDepuisTableauSQL(array $utilisateurTableau) : Utilisateur {
        return new Utilisateur(
            $utilisateurTableau["login"],
            $utilisateurTableau["nom"],
            $utilisateurTableau["prenom"],
        );
    }

    public static function recupererTrajetsCommePassager(Utilisateur $utilisateur): array
    {
        $sql = "SELECT * FROM trajet T JOIN passager P ON T.id = P.trajetId WHERE passagerLogin = :login";
        $valeurs = ["login" => $utilisateur->getLogin()];

        $pdoStatement = ConnexionBaseDeDonnees::getInstance()->getPdo()->prepare($sql);
        $pdoStatement->execute($valeurs);

        $utilisateurs = [];
        foreach($pdoStatement as $utilisateurFormatTableau) {
            $utilisateurs[] = Trajet::construireDepuisTableauSQL($utilisateurFormatTableau);
        }
        return $utilisateurs;
    }
    public static function supprimerParLogin($login){
        $sql = "DELETE FROM utilisateur WHERE login = :login";
        $pdoStatement = ConnexionBaseDeDonnees::getInstance()->getPdo()->prepare($sql);
        $valeur = ["login" => $login];
        $pdoStatement->execute($valeur);
    }



    protected function getNomTable(): string
    {
        return "utilisateur";
    }

    protected function getNomClePrimaire(): string
    {
        return "login";
    }

    protected function getNomsColonnes(): array
    {
        return ["login","nom","prenom"];
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array{
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
        );
    }




}