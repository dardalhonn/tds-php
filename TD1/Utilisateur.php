<?php
class Utilisateur {

    // Déclaration des attributs de classe comme string
    private string $login;
    private string $nom;
    private string $prenom;

    // Constructeur avec les types des arguments
    public function __construct(string $login, string $nom, string $prenom) {
        $this->login = substr($login, 0, 64);  // Limite à 64 caractères
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Getter pour le nom (retourne une chaîne de caractères)
    public function getNom() : string {
        return $this->nom;
    }

    // Setter pour le nom (prend une chaîne de caractères en paramètre)
    public function setNom(string $nom) : void {
        $this->nom = $nom;
    }

    // Getter pour le prénom (retourne une chaîne de caractères)
    public function getPrenom() : string {
        return $this->prenom;
    }

    // Setter pour le prénom (prend une chaîne de caractères en paramètre)
    public function setPrenom(string $prenom) : void {
        $this->prenom = $prenom;
    }

    // Getter pour le login (retourne une chaîne de caractères)
    public function getLogin() : string {
        return $this->login;
    }

    // Setter pour le login avec limitation à 64 caractères (prend une chaîne de caractères en paramètre)
    public function setLogin(string $login) : void {
        $this->login = substr($login, 0, 64);
    }

    // Convertir l'objet en chaîne de caractères
    public function __toString() : string {
        return "Utilisateur {$this->prenom} {$this->nom} de login {$this->login}";
    }


}
?>
