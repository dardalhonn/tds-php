<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Test Utilisateur</title>
</head>
<body>
<?php

require_once 'Utilisateur.php';

try {

    $utilisateur1 = new Utilisateur([], "Leblanc", "Juste");

    echo $utilisateur1;
} catch (TypeError $e) {

    echo "Erreur de type : " . $e->getMessage();
}
?>
</body>
</html>
