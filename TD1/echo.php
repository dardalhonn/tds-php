<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <title>Mon premier php</title>
</head>
<body>
Voici le résultat du script PHP :
<?php
$nom = "Leblanc";
$prenom = "Juste";
$login = "leblancj";


echo "<p>Utilisateur $prenom $nom de login $login</p>";

$utilisateur = array(
    "nom" => "Leblanc",
    "prenom" => "Juste",
    "login" => "leblancj"
);


var_dump($utilisateur);

echo "<p>Utilisateur {$utilisateur['prenom']} {$utilisateur['nom']} de login {$utilisateur['login']}</p>";


$utilisateurs  = array(
    array("nom" => "Leblanc", "prenom" => "Juste", "login" => "leblancj"),
    array("nom" => "Dupont", "prenom" => "Jean", "login" => "dupontj"),
    array("nom" => "Martin", "prenom" => "Pierre", "login" => "martinp")
);

// Affichage pour débogage
var_dump($utilisateurs);

if (!empty($utilisateurs)) {
    echo "<h2>Liste des utilisateurs :</h2>";
    echo "<ul>";
    foreach ($utilisateurs as $utilisateur) {
        echo "<li>Utilisateur {$utilisateur['prenom']} {$utilisateur['nom']} de login {$utilisateur['login']}</li>";
    }
    echo "</ul>";
} else {
    echo "<p>Il n'y a aucun utilisateur.</p>";
}
?>
</body>
</html>
