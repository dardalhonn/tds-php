<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MessageFlash;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        if (isset($_REQUEST['login'])) {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
            if (is_null($utilisateur)) {
                MessageFlash::ajouter("warning", "login inconnu");
                ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");
            }
            ControleurUtilisateur::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "détail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        } else {
            MessageFlash::ajouter("warning", "le login n'a pas étais préciser");
            ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Formulaire création", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }


    public static function creerDepuisFormulaire(): void
    {

        if (!isset($_REQUEST['login']) && !isset($_REQUEST['nom']) && !isset($_REQUEST['prenom']) && !isset($_REQUEST['mdp']) && !isset($_REQUEST['mdp2']) && !isset($_REQUEST['email'])) {
            MessageFlash::ajouter("warning", "il manque une/des donnée(s)");
            ControleurUtilisateur::redirectionVersURL("?action=afficherFormulaireCreation&controleur=utilisateur");
            return;
        }

        if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) {
            MessageFlash::ajouter("warning", "email pas bon");
            ControleurUtilisateur::redirectionVersURL("?action=afficherFormulaireCreation&controleur=utilisateur");
            return;
        }

        if ($_REQUEST['mdp'] != $_REQUEST['mdp2']) {
            MessageFlash::ajouter("warning", "Mots de passe distincts");
            ControleurUtilisateur::redirectionVersURL("?action=afficherFormulaireCreation&controleur=utilisateur");
            return;
        }

        $admin = 0;
        if (isset($_REQUEST['estAdmin']) && ConnexionUtilisateur::estAdministrateur()) {
            $admin = 1;
        }

        $utilisateur = ControleurUtilisateur::construireDepuisFormulaire(array("login" => $_REQUEST['login'], "nom" => $_REQUEST['nom'], "prenom" => $_REQUEST['prenom'], "mdp" => $_REQUEST['mdp'], "estAdmin" => $admin, "email" => "", "emailAValider" => $_REQUEST['email'], "nonce" => MotDePasse::genererChaineAleatoire()));
        VerificationEmail::envoiEmailValidation($utilisateur);
        (new UtilisateurRepository())->ajouter($utilisateur);
        MessageFlash::ajouter("success", "utilisateur creer");
        ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");
    }

    public static function validerEmail(): void
    {
        if (!isset($_REQUEST['login']) && !isset($_REQUEST['nonce'])) {
            MessageFlash::ajouter("warning", "pas de login ou nonce trouver");
            ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");
        }

        if (VerificationEmail::traiterEmailValidation($_REQUEST['login'], $_REQUEST['nonce'])) {
            MessageFlash::ajouter("warning", "erreur dans le traitement de la validation d'email");
            ControleurUtilisateur::redirectionVersURL("?action=afficherdetail&controleur=utilisateur&login=" . $_REQUEST['login']);
        } else {
            MessageFlash::ajouter("warning", "erreur dans le traitement de la validation d'email");
            ControleurUtilisateur::redirectionVersURL("?action=afficherliste&controleur=utilisateur");
        }
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "erreur", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }

    public static function supprimer(): void
    {
        if (isset($_REQUEST['login'])) {
            if (!ConnexionUtilisateur::estUtilisateur($_REQUEST['login'])) {
                MessageFlash::ajouter("danger", "vous ne pouvez supprimer un autre utilisateur que vous");
                ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");
                return;
            }
            $login = $_REQUEST['login'];
            $rep = (new UtilisateurRepository())->supprimer($login);
            if ($rep) {
                $utilisateurs = (new UtilisateurRepository())->recuperer();
                MessageFlash::ajouter("success", "L'utilisateur a bien été supprimé !");
                ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");
            } else {
                MessageFlash::ajouter("warning", "un problème est survenu lors de la suppression");
                ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");
            }

        } else {
            MessageFlash::ajouter("warning", "le login n'est pas préciser");
            ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");
        }

    }


    public static function afficherFormulaireMiseAJour()
    {
        if (!isset($_REQUEST['login'])) {
            MessageFlash::ajouter("warning", "Login inconnu");
            ControleurUtilisateur::redirectionVersURL("?action=afficherliste&controleur=utilisateur");
            return;
        }

        if (!ConnexionUtilisateur::estUtilisateur($_REQUEST['login'])) {
            if (!ConnexionUtilisateur::estAdministrateur()) {
                MessageFlash::ajouter("danger", "La mise à jour n’est possible que pour l’utilisateur connecté");
                ControleurUtilisateur::redirectionVersURL("?action=afficherliste&controleur=utilisateur");
                return;
            }
        }

        $login = $_REQUEST['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if (is_null($utilisateur)) {
            MessageFlash::ajouter("warning", "Login inconnu");
            ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");
        } else {
            ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "utilisateurFormulaireMAJ", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php", 'utilisateur' => $utilisateur, "login" => $login]);
        }


    }


    public static function afficherFormulaireConnexion()
    {
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "formulaire Connexion", "cheminCorpsVue" => 'utilisateur/formulaireConnexion.php']);
    }

    public static function connecter()
    {
        if (!isset($_REQUEST['login']) && !isset($_REQUEST['mdp'])) {
            MessageFlash::ajouter("danger", "Login et/ou mot de passe manquant");
            ControleurUtilisateur::redirectionVersURL("?action=afficherFormulaireConnexion&controleur=utilisateur");
            return;
        }

        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
        if (is_null($utilisateur)) {
            MessageFlash::ajouter("danger", "Login inconnu");
            ControleurUtilisateur::redirectionVersURL("?action=afficherFormulaireConnexion&controleur=utilisateur");
            return;
        }

        if (!VerificationEmail::aValideEmail($utilisateur)) {
            MessageFlash::ajouter("warning", "vous n'avez pas valider votre email");
            ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");
            return;
        }

        if (!MotDePasse::verifier($_REQUEST['mdp'], $utilisateur->getMdpHache())) {
            MessageFlash::ajouter("danger", "Mot de passe incorrect");
            ControleurUtilisateur::redirectionVersURL("?action=afficherFormulaireConnexion&controleur=utilisateur");
        }



            ConnexionUtilisateur::connecter($_REQUEST['login']);
            MessageFlash::ajouter("success", "utilisateur connecter");
            ControleurUtilisateur::redirectionVersURL("?action=afficherdetail&controleur=utilisateur&login=" . urlencode($_REQUEST['login']));
    }


    public static function deconnecter()
    {
        ConnexionUtilisateur::deconnecter();
        MessageFlash::ajouter("info", "utilisateur deconnecter");
        ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");
    }

    public static function mettreAJour()
    {
        if (!isset($_REQUEST['login']) && !isset($_REQUEST['nom']) && !isset($_REQUEST['prenom']) && !isset($_REQUEST['mdp']) && !isset($_REQUEST['mdp2']) && !isset($_REQUEST['mdpa']) && !isset($_REQUEST['email'])) {
            MessageFlash::ajouter("warning", "il manque une/des donnée(s)");
            ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");
            return;
        }

        if (!ConnexionUtilisateur::estUtilisateur($_REQUEST['login'])) {
            if (!ConnexionUtilisateur::estAdministrateur()) {
                MessageFlash::ajouter("warning", "La mise à jour n’est possible que pour l’utilisateur connecté");
                ControleurUtilisateur::redirectionVersURL("?action=afficherFormulaireMiseAJour&controleur=utilisateur&login=" . $_REQUEST['login']);
                return;
            }
        }

        if ($_REQUEST['mdp'] != $_REQUEST['mdp2']) {
            MessageFlash::ajouter("warning", "mot de passe distinct");
            ControleurUtilisateur::redirectionVersURL("?action=afficherFormulaireMiseAJour&controleur=utilisateur&login=" . $_REQUEST['login']);
            return;
        }

        /* @var Utilisateur $utilisateur */
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
        if (is_null($utilisateur)) {
            MessageFlash::ajouter("warning", "Login inconnu");
            ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");
            return;
        }

        if ($utilisateur->getEmail() != $_REQUEST['email']) {
            if (!filter_var($_REQUEST['email'], FILTER_VALIDATE_EMAIL)) {
                MessageFlash::ajouter("warning", "email pas bon");
                ControleurUtilisateur::redirectionVersURL("?action=afficherFormulaireMiseAJour&controleur=utilisateur&login=" . $_REQUEST['login']);
                return;
            }
            $utilisateur->setEmail("");
            $utilisateur->setEmailAValider($_REQUEST['email']);
            $utilisateur->setNonce(MotDePasse::genererChaineAleatoire());
            VerificationEmail::envoiEmailValidation($utilisateur);
            ConnexionUtilisateur::deconnecter();
        }

        if (!ConnexionUtilisateur::estAdministrateur()) {
            if (!MotDePasse::verifier($_REQUEST['mdpa'], $utilisateur->getMdpHache())) {
                MessageFlash::ajouter("warning", "Ancien mot de passe erroné.");
                ControleurUtilisateur::redirectionVersURL("?action=afficherFormulaireMiseAJour&controleur=utilisateur&login=" . $_REQUEST['login']);
                return;
            }
        }

        $admin = 0;
        if (isset($_REQUEST["estAdmin"]) && ConnexionUtilisateur::estAdministrateur()) {
            $admin = 1;
        }

        $utilisateur->setLogin($_REQUEST['login']);
        $utilisateur->setNom($_REQUEST['nom']);
        $utilisateur->setPrenom($_REQUEST['prenom']);
        $mdp = MotDePasse::hacher($_REQUEST["mdp"]);
        $utilisateur->setMdpHache($mdp);
        $utilisateur->setEstAdmin($admin);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();

        MessageFlash::ajouter("success", "utilisateur mis a jour");
        ControleurUtilisateur::redirectionVersURL("?action=afficherListe&controleur=utilisateur");

    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $mdp = MotDePasse::hacher($tableauDonneesFormulaire["mdp"]);

        return new Utilisateur($tableauDonneesFormulaire["login"], $tableauDonneesFormulaire["nom"], $tableauDonneesFormulaire["prenom"], $mdp, $tableauDonneesFormulaire["estAdmin"], $tableauDonneesFormulaire['email'], $tableauDonneesFormulaire['emailAValider'], $tableauDonneesFormulaire['nonce']);
    }


}