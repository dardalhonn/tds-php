<?php


/** @var Trajet $trajet */
$id = $trajet->getId();
$depart = htmlspecialchars($trajet->getDepart());
$arivee = htmlspecialchars($trajet->getArrivee());
$prix = $trajet->getPrix();
$date = $trajet->getDate()->format('Y-m-d');
$conducteur = htmlspecialchars($trajet->getConducteur()->getLogin());

if ($trajet->isNonFumeur()){
    $fumeur = "oui";
}else{
    $fumeur = "non";
}

?>

<h1>Detail trajet: </h1>
<lu>
    <li> id : <?= $id ?></li>
    <li> depart : <?= $depart ?></li>
    <li> arrivée : <?= $arivee?></li>
    <li> date : <?= $date ?></li>
    <li> prix : <?= $prix ?></li>
    <li> fumeur : <?= $fumeur ?> </li>
    <li> Conducteur : <?= $conducteur?></li>
</lu>
