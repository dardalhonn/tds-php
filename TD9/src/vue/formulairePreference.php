<form method="<?= \App\Covoiturage\Configuration\ConfigurationSite::getDebug()?"post":"get"; ?>" action="/tds-php/TD9/web/controleurFrontal.php">
    <fieldset>
        <input type="hidden" name="action" value="enregistrerPreference">
        <input type="radio" id="utilisateurId" name="controleur_defaut" value="utilisateur"
            <?php use App\Covoiturage\Lib\PreferenceControleur;
            if (PreferenceControleur::existe()){
                if (PreferenceControleur::lire() == "utilisateur"){
                    echo "checked";
                }
            } ?>
        >
        <label for="utilisateurId">Utilisateur</label>
        <input type="radio" id="trajetId" name="controleur_defaut" value="trajet"
        <?php

        if (PreferenceControleur::existe()){
            if (PreferenceControleur::lire() == "trajet"){
                echo "checked";
            }
        } ?>
        >
        <label for="trajetId">Trajet</label>
        <input type="submit" value="Envoyer"/>
    </fieldset>
</form>