<form method="<?= \App\Covoiturage\Configuration\ConfigurationSite::getDebug()?"post":"get"; ?>" action="ControleurFrontal.php">
    <fieldset>
        <legend> Formulaire Connexion :</legend>
        <input type='hidden' name='action' value='connecter'>
        <input type='hidden' name='controleur' value='utilisateur'>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" placeholder="leblancj" name="login" id="login_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <input type="submit" value="Envoyer"/>
    </fieldset>
</form>