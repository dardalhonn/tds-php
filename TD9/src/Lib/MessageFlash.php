<?php

namespace App\Covoiturage\Lib;

use App\Covoiturage\Modele\HTTP\Session;

class MessageFlash
{

// Les messages sont enregistrés en session associée à la clé suivante
    private static string $cleFlash = "_messagesFlash";

// $type parmi "success", "info", "warning" ou "danger"
    public static function ajouter(string $type, string $message): void
    {
        $tousMessages = MessageFlash::lireTousMessages();
        $tousMessages[$type][] = $message;
        Session::getInstance()->enregistrer(MessageFlash::$cleFlash, $tousMessages);
    }

    public static function contientMessage(string $type): bool
    {
        if (Session::getInstance()->contient(MessageFlash::$cleFlash)) {
            $messageFlash = Session::getInstance()->lire(MessageFlash::$cleFlash);
        }
        if (is_null($messageFlash[$type])){
            return false;
        }
        return true;
    }

// Attention : la lecture doit détruire le message
    public static function lireMessages(string $type): array
    {
        if (Session::getInstance()->contient(MessageFlash::$cleFlash)){
        $messageFlash = Session::getInstance()->lire(MessageFlash::$cleFlash);
        $messageType = $messageFlash[$type];
        $messageFlash[$type] = [];
        Session::getInstance()->supprimer(MessageFlash::$cleFlash);
        Session::getInstance()->enregistrer(MessageFlash::$cleFlash, $messageFlash);
        }else{
        $messageType = [];
        }
        return $messageType;
    }

    public static function lireTousMessages(): array
    {

        return [
            "success" => MessageFlash::lireMessages("success"),
            "info" => MessageFlash::lireMessages("info"),
            "warning" => MessageFlash::lireMessages("warning"),
            "danger" => MessageFlash::lireMessages("warning")
        ];
    }

}