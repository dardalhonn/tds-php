<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Détails de l'utilisateur</title>
</head>
<body>
<?php
if (isset($utilisateurEnParametre)) :
    $nomHTML = htmlspecialchars($utilisateurEnParametre->getNom(), ENT_QUOTES, 'UTF-8');
    $prenomHTML = htmlspecialchars($utilisateurEnParametre->getPrenom(), ENT_QUOTES, 'UTF-8');
    $loginHTML = htmlspecialchars($utilisateurEnParametre->getLogin(), ENT_QUOTES, 'UTF-8');

    echo "Nom : " . $nomHTML . "<br>";
    echo "Prénom : " . $prenomHTML . "<br>";
    echo "Login : " . $loginHTML;
else:
    echo "Utilisateur introuvable.";
endif;
?>
</body>
</html>
