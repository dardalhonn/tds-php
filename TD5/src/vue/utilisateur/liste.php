<!-- TD5/vue/utilisateur/liste.php -->
<?php
/** @var ModeleUtilisateur[] $utilisateurs */

foreach ($utilisateurs as $utilisateur) {
    // Échapper le login pour le HTML
    $loginHTML = htmlspecialchars($utilisateur->getLogin());
    // Encoder le login pour l'URL
    $loginURL = rawurlencode($utilisateur->getLogin());

    echo '<p>Utilisateur de login <a href="http://localhost/tds-php/TD5/web/controleurFrontal.php?action=afficherDetail&login=' . $loginURL . '">' . $loginHTML . '</a></p>';
}
?>
