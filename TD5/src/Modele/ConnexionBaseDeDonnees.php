<?php

namespace App\Covoiturage\Modele;

use App\Covoiturage\Configuration\ConfigurationBaseDeDonnees;
use PDO; // Importation de la classe PDO
use PDOException; // Importation de l'exception PDO

class ConnexionBaseDeDonnees {
    private static ?self $instance = null; // Utilisation de self au lieu de ConnexionBaseDeDonnees
    private PDO $PDO;

    // Constructeur privé pour le singleton
    private function __construct() {
        $nomHote = ConfigurationBaseDeDonnees::getNomHote();
        $port = ConfigurationBaseDeDonnees::getPort();
        $nomBaseDeDonnees = ConfigurationBaseDeDonnees::getNomBaseDeDonnees();
        $login = ConfigurationBaseDeDonnees::getLogin();
        $motDePasse = ConfigurationBaseDeDonnees::getPassWord();

        $this->PDO = new PDO(
            "mysql:host=$nomHote;port=$port;dbname=$nomBaseDeDonnees",
            $login,
            $motDePasse,
            [PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"]
        );

        // On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
        $this->PDO->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    }

    // Méthode statique pour obtenir l'instance
    private static function getInstance(): self {
        if (is_null(self::$instance)) {
            self::$instance = new self(); // Utilisation de self pour le singleton
        }
        return self::$instance;
    }

    public static function getPdo(): PDO {
        return self::getInstance()->PDO; // Appel à la méthode de l'instance
    }
}
