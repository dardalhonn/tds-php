<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Utilisateur; // Importation correcte
use App\Covoiturage\Modele\ConnexionBaseDeDonnees; // Importation de ConnexionBaseDeDonnees si besoin

class ControleurUtilisateur
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = Utilisateur::recupererUtilisateurs(); // Appel au modèle pour gérer la BD
        // Appel à la vue générale
        self::afficherVue('vueGenerale.php', [
            'titre' => 'Liste des utilisateurs',
            'cheminCorpasVue' => 'utilisateur/liste.php', // Chemin vers la vue spécifique
            'utilisateurs' => $utilisateurs // Vous pouvez passer les utilisateurs si nécessaire
        ]);
    }

#dadazdada
    public static function afficherDetail(): void
    {
        if (!isset($_GET['login']) || empty($_GET['login'])) {
            self::afficherVue('vueGenerale.php', [
                'titre' => 'Erreur',
                'cheminCorpsVue' => 'utilisateur/erreur.php',
            ]);
            return;
        }

        $login = $_GET['login'];
        $utilisateur = Utilisateur::recupererUtilisateurParLogin($login);

        if (empty($utilisateur)) {
            self::afficherVue('vueGenerale.php', [
                'titre' => 'Erreur',
                'cheminCorpsVue' => 'utilisateur/erreur.php',
            ]);
        } else {
            self::afficherVue('vueGenerale.php', [
                'titre' => 'Détails de l\'utilisateur',
                'cheminCorpsVue' => 'utilisateur/detail.php',
                'utilisateurEnParametre' => $utilisateur,
            ]);
        }
    }


    public static function afficherFormulaireCreation(): void
    {
        // Afficher la vue du formulaire de création
        self::afficherVue('utilisateur/formulaireCreation.php');
    }
    /** @var Utilisateur[] $utilisateurs */


    public static function creerDepuisFormulaire() {
        // Récupérer les données du formulaire
        $login = $_GET['login'] ?? '';
        $nom = $_GET['nom'] ?? '';
        $prenom = $_GET['prenom'] ?? '';

        // Créer un nouvel utilisateur avec les données fournies
        $nouvelUtilisateur = new Utilisateur($login, $nom, $prenom);

        // Ajouter l'utilisateur à la base de données
        $nouvelUtilisateur->ajouter();

        // Récupérer la liste mise à jour des utilisateurs
        /** @var Utilisateur[] $utilisateurs */
        $utilisateurs = Utilisateur::recupererUtilisateurs(); // Assurez-vous que cette méthode renvoie bien un tableau d'utilisateurs

        // Charger la vue utilisateurCree.php
        $titre = "Utilisateur créé"; // Titre de la page
        $cheminCorpsVue = "utilisateur/utilisateurCree.php"; // Chemin vers votre nouvelle vue

        // Passez la liste d'utilisateurs à la vue générale
        require __DIR__ . '/../vue/vueGenerale.php'; // Cela inclura également la variable $utilisateurs dans la vue générale
    }




    // Méthode privée pour afficher les vues
    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . '/../vue/' . $cheminVue; // Utiliser un chemin absolu pour la vue
    }
}
