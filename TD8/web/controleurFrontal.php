<?php

require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

use App\Covoiturage\Controleur\ControleurUtilisateur;
use App\Covoiturage\Lib\PreferenceControleur;
use App\Covoiturage\Modele\HTTP\Cookie;


// initialisation en activant l'affichage de débogage
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();
// enregistrement d'une association "espace de nom" → "dossier"
$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');




// On récupère l'action passée dans l'URL
if (!isset($_REQUEST['controleur'])){
    if (PreferenceControleur::existe()){
        $controleur = "App\Covoiturage\Controleur\Controleur" . ucfirst(Cookie::lire('preferenceControleur'));
    }else{
    $controleur = "App\Covoiturage\Controleur\ControleurUtilisateur";
    }
}else{
    $controleur = "App\Covoiturage\Controleur\Controleur" . ucfirst($_REQUEST['controleur']);
}
if (isset($_REQUEST['action'])) {
    $action = $_REQUEST['action'];

    if (class_exists($controleur)){
        if (in_array($action, get_class_methods($controleur))){
            $controleur::$action();
        }else{
            $controleur::afficherErreur("l'action ne correspond a aucune des fonctions");
        }
    }else{
        ControleurUtilisateur::afficherErreur("la classe n'existe pas <br>" . $controleur);
    }

}else{
    $controleur::afficherListe();
}

?>