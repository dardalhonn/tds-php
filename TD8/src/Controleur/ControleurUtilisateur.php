<?php

namespace App\Covoiturage\Controleur;

use App\Covoiturage\Lib\ConnexionUtilisateur;
use App\Covoiturage\Lib\MotDePasse;
use App\Covoiturage\Lib\VerificationEmail;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\HTTP\Cookie;
use App\Covoiturage\Modele\HTTP\Session;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;

class ControleurUtilisateur extends ControleurGenerique
{
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe(): void
    {
        $utilisateurs = (new UtilisateurRepository())->recuperer(); //appel au modèle pour gérer la BD
        ControleurUtilisateur::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "Liste des utilisateurs", "cheminCorpsVue" => "utilisateur/liste.php"]);  //"redirige" vers la vue
    }

    public static function afficherDetail(): void
    {
        if (isset($_REQUEST['login'])) {
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
            ControleurUtilisateur::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "détail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        } else {
            $messageErreur = "le login n'a pas étais préciser";
            ControleurUtilisateur::afficherErreur($messageErreur);
        }
    }

    public static function afficherFormulaireCreation(): void
    {
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "Formulaire création", "cheminCorpsVue" => "utilisateur/formulaireCreation.php"]);
    }


    public static function creerDepuisFormulaire(): void
    {

        if (!isset($_REQUEST['login']) && !isset($_REQUEST['nom']) && !isset($_REQUEST['prenom']) && !isset($_REQUEST['mdp']) && !isset($_REQUEST['mdp2']) && !isset($_REQUEST['email'])) {
            ControleurUtilisateur::afficherErreur("il manque une/des donnée(s)");
            return;
        }

        if (!filter_var($_REQUEST['email'],FILTER_VALIDATE_EMAIL)){
            ControleurUtilisateur::afficherErreur("email pas bon");
            return;
        }

        if ($_REQUEST['mdp'] != $_REQUEST['mdp2']) {
            ControleurUtilisateur::afficherErreur("Mots de passe distincts");
            return;
        }

        $admin = 0;
        if (isset($_REQUEST['estAdmin']) && ConnexionUtilisateur::estAdministrateur()) {
            $admin = 1;
        }

        $utilisateur = ControleurUtilisateur::construireDepuisFormulaire(array("login" => $_REQUEST['login'], "nom" => $_REQUEST['nom'], "prenom" => $_REQUEST['prenom'], "mdp" => $_REQUEST['mdp'], "estAdmin" => $admin, "email" => "", "emailAValider" => $_REQUEST['email'], "nonce" => MotDePasse::genererChaineAleatoire()));
        VerificationEmail::envoiEmailValidation($utilisateur);
        (new UtilisateurRepository())->ajouter($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ControleurUtilisateur::afficherVue('vueGenerale.php', ['utilisateurs' => $utilisateurs, "titre" => "utilisateurCree", "cheminCorpsVue" => "utilisateur/utilisateurCree.php"]);
    }

    public static function validerEmail():void {
        if (!isset($_REQUEST['login']) && !isset($_REQUEST['nonce'])){
            ControleurUtilisateur::afficherErreur("pas de login ou nonce trouver");
        }

        if(VerificationEmail::traiterEmailValidation($_REQUEST['login'], $_REQUEST['nonce'])){
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
            ControleurUtilisateur::afficherVue('vueGenerale.php', ['utilisateur' => $utilisateur, "titre" => "détail utilisateur", "cheminCorpsVue" => "utilisateur/detail.php"]);
        }else{
            ControleurUtilisateur::afficherErreur("erreur dans le traitement de la validation d'email");
        }
    }

    public static function afficherErreur(string $messageErreur = ""): void
    {

        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "erreur", "cheminCorpsVue" => "utilisateur/erreur.php", "messageErreur" => $messageErreur]);
    }

    public static function supprimer(): void
    {
        if (isset($_REQUEST['login'])) {
            if (!ConnexionUtilisateur::estUtilisateur($_REQUEST['login'])) {
                ControleurUtilisateur::afficherErreur("vous ne pouvez supprimer un autre utilisateur que vous");
                return;
            }
            $login = $_REQUEST['login'];
            $rep = (new UtilisateurRepository())->supprimer($login);
            if ($rep) {
                $utilisateurs = (new UtilisateurRepository())->recuperer();
                ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "utilisateurSupprime", "cheminCorpsVue" => "utilisateur/utilisateurSupprime.php", 'utilisateurs' => $utilisateurs, "login" => $login]);
            } else {
                ControleurUtilisateur::afficherErreur("un problème est survenu lors de la suppression");
            }

        } else {
            ControleurUtilisateur::afficherErreur("le login n'est pas préciser");
        }

    }


    public static function afficherFormulaireMiseAJour()
    {
        if (!isset($_REQUEST['login'])) {
            ControleurUtilisateur::afficherErreur("le login n'est pas préciser");
            return;
        }

        if (!ConnexionUtilisateur::estUtilisateur($_REQUEST['login'])) {
            if (!ConnexionUtilisateur::estAdministrateur()) {
                ControleurUtilisateur::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté");
                return;
            }
        }

        $login = $_REQUEST['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if (is_null($utilisateur)) {
            ControleurUtilisateur::afficherErreur("Login inconnu");
        } else {
            ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "utilisateurFormulaireMAJ", "cheminCorpsVue" => "utilisateur/formulaireMiseAJour.php", 'utilisateur' => $utilisateur, "login" => $login]);
        }


    }


    public static function afficherFormulaireConnexion()
    {
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "formulaire Connexion", "cheminCorpsVue" => 'utilisateur/formulaireConnexion.php']);
    }

    public static function connecter()
    {
        if (!isset($_REQUEST['login']) && !isset($_REQUEST['mdp'])) {
            ControleurUtilisateur::afficherErreur("Login et/ou mot de passe manquant");
            return;
        }

            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
        if (is_null($utilisateur)){
            ControleurUtilisateur::afficherErreur("utilisateur inconnu au bataillon");
            return;
        }

        if (!VerificationEmail::aValideEmail($utilisateur)){
            ControleurUtilisateur::afficherErreur("vous n'avez pas valider votre email");
            return;
        }

            if (MotDePasse::verifier($_REQUEST['mdp'], $utilisateur->getMdpHache())) {
                ConnexionUtilisateur::connecter($_REQUEST['login']);
                ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "utilisateurConnecte", "cheminCorpsVue" => 'utilisateur/utilisateurConnecte.php', 'utilisateur' => $utilisateur]);
            } else {
                ControleurUtilisateur::afficherErreur("Login et/ou mot de passe incorrect");
            }
    }


    public static function deconnecter()
    {
        ConnexionUtilisateur::deconnecter();
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "utilisateurDeconnecte", "cheminCorpsVue" => "utilisateur/utilisateurDeconnecte.php", 'utilisateurs' => $utilisateurs]);
    }

    public static function mettreAJour()
    {
        if (!isset($_REQUEST['login']) && !isset($_REQUEST['nom']) && !isset($_REQUEST['prenom']) && !isset($_REQUEST['mdp']) && !isset($_REQUEST['mdp2']) && !isset($_REQUEST['mdpa']) && !isset($_REQUEST['email'])) {
            ControleurUtilisateur::afficherErreur("il manque une/des donnée(s)");
            return;
        }

        if (!ConnexionUtilisateur::estUtilisateur($_REQUEST['login'])) {
            if (!ConnexionUtilisateur::estAdministrateur()){
            ControleurUtilisateur::afficherErreur("La mise à jour n’est possible que pour l’utilisateur connecté");
            return;
            }
        }

        if ($_REQUEST['mdp'] != $_REQUEST['mdp2']) {
            ControleurUtilisateur::afficherErreur("mot de passe distinct");
            return;
        }

        /* @var Utilisateur $utilisateur */
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($_REQUEST['login']);
        if (is_null($utilisateur)) {
            ControleurUtilisateur::afficherErreur("Login inconnu");
            return;
        }

        if ($utilisateur->getEmail() != $_REQUEST['email']){
            if (!filter_var($_REQUEST['email'],FILTER_VALIDATE_EMAIL)){
                ControleurUtilisateur::afficherErreur("email pas bon");
                return;
            }
            $utilisateur->setEmail("");
            $utilisateur->setEmailAValider($_REQUEST['email']);
            $utilisateur->setNonce(MotDePasse::genererChaineAleatoire());
            VerificationEmail::envoiEmailValidation($utilisateur);
            ConnexionUtilisateur::deconnecter();
        }

        if (!ConnexionUtilisateur::estAdministrateur()) {
            if (!MotDePasse::verifier($_REQUEST['mdpa'], $utilisateur->getMdpHache())) {
                ControleurUtilisateur::afficherErreur("mot de passe incorrect");
                return;
            }
        }

        $admin = 0;
        if (isset($_REQUEST["estAdmin"]) && ConnexionUtilisateur::estAdministrateur()) {
            $admin = 1;
        }

        $utilisateur->setLogin($_REQUEST['login']);
        $utilisateur->setNom($_REQUEST['nom']);
        $utilisateur->setPrenom($_REQUEST['prenom']);
        $mdp = MotDePasse::hacher($_REQUEST["mdp"]);
        $utilisateur->setMdpHache($mdp);
        $utilisateur->setEstAdmin($admin);
        (new UtilisateurRepository())->mettreAJour($utilisateur);
        $utilisateurs = (new UtilisateurRepository())->recuperer();

        ControleurUtilisateur::afficherVue('vueGenerale.php', ["titre" => "utilisateurMiseAJour", "cheminCorpsVue" => "utilisateur/utilisateurMisAJour.php", "login" => $utilisateur->getLogin(), 'utilisateurs' => $utilisateurs]);


    }

    private static function construireDepuisFormulaire(array $tableauDonneesFormulaire): Utilisateur
    {
        $mdp = MotDePasse::hacher($tableauDonneesFormulaire["mdp"]);

        return new Utilisateur($tableauDonneesFormulaire["login"], $tableauDonneesFormulaire["nom"], $tableauDonneesFormulaire["prenom"], $mdp, $tableauDonneesFormulaire["estAdmin"], $tableauDonneesFormulaire['email'], $tableauDonneesFormulaire['emailAValider'], $tableauDonneesFormulaire['nonce']);
    }


}

?>