<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use DateTime;

class TrajetRepository extends AbstractRepository
{

    /**
     * @return Utilisateur[]
     */
    private function recupererPassagers(Trajet $trajet) : array {
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare("SELECT p.passagerLogin FROM utilisateur u INNER JOIN passager p ON u.login = p.passagerLogin WHERE trajetid = :id");

        $pdoStatement->execute(array("id"=> $trajet->getId()));

        $utilisateurs = [];

        foreach ($pdoStatement as $utilisateur){
            $utilisateurs[] = (new UtilisateurRepository())->recupererParClePrimaire($utilisateur['passagerLogin']);
        }

        return $utilisateurs;
    }


    protected function construireDepuisTableauSQL(array $trajetTableau) : Trajet {
        $trajet = new Trajet(
            $trajetTableau["id"],
            $trajetTableau["depart"],
            $trajetTableau["arrivee"],
            new DateTime($trajetTableau["date"]),
            $trajetTableau["prix"],
            (new UtilisateurRepository())->recupererParClePrimaire($trajetTableau["conducteurLogin"]),
            $trajetTableau["nonFumeur"]
        );

        return $trajet;
    }

    protected function getNomTable(): string
    {
        return "trajet";
    }

    protected function getNomClePrimaire(): string
    {
        return "id";
    }

    /** @return string[] */
    protected function getNomsColonnes(): array
    {
        return ["id", "depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"];
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array
    {
        /** @var Trajet $trajet */
        $fumeur = $trajet->isNonFumeur()?1:0;

        return array(
            "idTag" => $trajet->getId(),
            "departTag" => $trajet->getDepart(),
            "arriveeTag" => $trajet->getArrivee(),
            "dateTag" => $trajet->getDate()->format("Y-m-d"),
            "prixTag" => $trajet->getPrix(),
            "conducteurLoginTag" => $trajet->getConducteur()->getLogin(),
            "nonFumeurTag" => $fumeur,

        );
    }
}