<?php
/** @var \App\Covoiturage\Modele\DataObject\Utilisateur $utilisateur */
?>

<form method="<?= \App\Covoiturage\Configuration\ConfigurationSite::getDebug()?"post":"get"; ?>" action="/tds-php/TD8/web/controleurFrontal.php">
    <fieldset>
        <legend>Mon formulaire :</legend>
        <input type='hidden' name='action' value='mettreAJour'>
        <input type='hidden' name='controleur' value='utilisateur'>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="login_id">Login&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getLogin()) ?>" name="login" id="login_id" readonly/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="nom_id">nom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getNom()) ?>" name="nom" id="nom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="prenom_id">prénom&#42;</label>
            <input class="InputAddOn-field" type="text" value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>" name="prenom" id="prenom_id" required/>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdpa" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp_id">Nouveau Mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp" id="mdp_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="mdp2_id">Vérification du nouveau mot de passe&#42;</label>
            <input class="InputAddOn-field" type="password" value="" placeholder="" name="mdp2" id="mdp2_id" required>
        </p>
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="email_id">Email&#42;</label>
            <input class="InputAddOn-field" type="email" value="<?= $utilisateur->getEmail() ?>" name="email" id="email_id" required>
        </p>

        <?php
        if (\App\Covoiturage\Lib\ConnexionUtilisateur::estAdministrateur()){
            echo '
        <p class="InputAddOn">
            <label class="InputAddOn-item" for="estAdmin_id">Administrateur</label>
            <input class="InputAddOn-field" type="checkbox" placeholder="" name="estAdmin" id="estAdmin_id" ' . ($utilisateur->isEstAdmin()?'checked':'') . '></p>';
        } ?>

        <p>
            <input type="submit" value="Envoyer"/>
        </p>
    </fieldset>
</form>


