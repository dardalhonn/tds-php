<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" href="/tds-php/TD8/ressources/css/navstyle.css">
    <title><?php /** @var string $titre */ echo $titre; ?></title>
</head>
<body>
<header>
    <nav>
        <ul>
            <li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=utilisateur">Gestion des utilisateurs</a>
            </li><li>
                <a href="controleurFrontal.php?action=afficherListe&controleur=trajet">Gestion des trajets</a>
            </li>
            <li>
                <a href="?action=afficherFormulairePreference"> <img src="../ressources/img/heart.png"> </a>
            </li>
            <?php if (!\App\Covoiturage\Lib\ConnexionUtilisateur::estConnecte()){
                echo '<li><a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireCreation"><img src="../ressources/img/add-user.png"></a></li>             
                      <li><a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireConnexion"><img src="../ressources/img/enter.png"></a></li>';
            }else{
                echo '<li><a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=' . rawurlencode(\App\Covoiturage\Lib\ConnexionUtilisateur::getLoginUtilisateurConnecte()) . '"><img src="../ressources/img/user.png"></a></li>
                      <li><a href="controleurFrontal.php?controleur=utilisateur&action=deconnecter"><img src="../ressources/img/logout.png"></a></li>';
            }
            ?>
        </ul>
    </nav>
</header>
<main>
    <?php
    /** @var string $cheminCorpsVue */
    require __DIR__ . "/{$cheminCorpsVue}";
    ?>
</main>
<footer>
    <p>
        Site de covoiturage de Ruiz paul
    </p>
</footer>
</body>
</html>