<?php
/** @var Trajet $trajet */

$depart = htmlspecialchars($trajet->getDepart());
$arrivee = htmlspecialchars($trajet->getArrivee());
$date = $trajet->getDate()->format("Y-m-d");
$conducteurLogin = htmlspecialchars($trajet->getConducteur()->getLogin());


?>

<!DOCTYPE html>
<html>
<body>
<div>
    <form method="<?= \App\Covoiturage\Configuration\ConfigurationSite::getDebug()?"post":"get"; ?>" action="controleurFrontal.php">
        <fieldset>
            <legend>Mon formulaire :</legend>
            <input type='hidden' name='action' value='mettreAJour'>
            <input type='hidden' name='controleur' value='trajet'>
            <input type='hidden' name='id' value='<?= $trajet->getId() ?>'>
            <p>
                <label for="depart_id">Depart</label> :
                <input type="text" value="<?= $depart ?>" name="depart" id="depart_id" required/>
            </p>
            <p>
                <label for="arrivee_id">Arrivée</label> :
                <input type="text" value="<?= $arrivee ?>" name="arrivee" id="arrivee_id" required/>
            </p>
            <p>
                <label for="date_id">Date</label> :
                <input type="date" value="<?= $date ?>" name="date" id="date_id"  required/>
            </p>
            <p>
                <label for="prix_id">Prix</label> :
                <input type="int" value="<?= $trajet->getPrix() ?>" name="prix" id="prix_id"  required/>
            </p>
            <p>
                <label for="conducteurLogin_id">Login du conducteur</label> :
                <input type="text" value="<?= $conducteurLogin ?>" name="conducteurLogin" id="conducteurLogin_id" required/>
            </p>
            <p>
                <label for="nonFumeur_id">Non Fumeur ?</label> :
                <input type="checkbox" name="nonFumeur" id="nonFumeur_id" <?= $trajet->isNonFumeur() ? " checked" : "";?> />
            </p>
            <p>
                <input type="submit" value="Envoyer" />
            </p>
        </fieldset>
    </form>
</div>
</body>
</html>
