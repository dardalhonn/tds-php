<?php

use App\Covoiturage\Controleur\ControleurUtilisateur;

// Chargement de l'autoloader
require_once __DIR__ . '/../src/Lib/Psr4AutoloaderClass.php';

// Initialisation de l'autoloader
$chargeurDeClasse = new App\Covoiturage\Lib\Psr4AutoloaderClass(false);
$chargeurDeClasse->register();


$chargeurDeClasse->addNamespace('App\Covoiturage', __DIR__ . '/../src');

$methodes = get_class_methods('App\Covoiturage\Controleur\ControleurUtilisateur');
$action = $_GET['action'] ?? 'afficherListe';


$controleur = $_GET['controleur'] ?? 'utilisateur';


$nomDeClasseControleur = 'App\\Covoiturage\\Controleur\\Controleur' . ucfirst($controleur);

// Vérifier si la classe existe
if (class_exists($nomDeClasseControleur)) {
    // Vérifier si l'action existe dans cette classe
    if (method_exists($nomDeClasseControleur, $action)) {
        // Appeler dynamiquement l'action sur le contrôleur
        $nomDeClasseControleur::$action();
    } else {
        // Appeler l'action afficherErreur si l'action n'existe pas
        App\Covoiturage\Controleur\ControleurUtilisateur::afficherErreur("L'action '$action' n'existe pas.");
    }
} else {
    // Appeler l'action afficherErreur si le contrôleur n'existe pas
    App\Covoiturage\Controleur\ControleurUtilisateur::afficherErreur("Le contrôleur '$controleur' n'existe pas.");
}

// controleurFrontal.php
