<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\AbstractRepository;
use PDO;

class UtilisateurRepository extends AbstractRepository {

    protected function getNomTable(): string
    {
        return "utilisateur";
    }

    /** @return string[] */
    protected function getNomsColonnes(): array {
        return ["login", "nom", "prenom"];
    }


    protected function getNomClePrimaire(): string {
        return "login"; 
    }

    public static function supprimerParLogin(string $login): void {
        $sql = "DELETE FROM utilisateur WHERE login = :loginTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute(['loginTag' => $login]);
    }
    

    




    protected function construireDepuisTableauSQL(array $utilisateurFormatTableau): Utilisateur {
        return new Utilisateur(
            $utilisateurFormatTableau['login'] ?? '',
            $utilisateurFormatTableau['nom'] ?? '',
            $utilisateurFormatTableau['prenom'] ?? ''
        );
    }

    protected function formatTableauSQL(AbstractDataObject $utilisateur): array {
        /** @var Utilisateur $utilisateur */
        return array(
            "loginTag" => $utilisateur->getLogin(),
            "nomTag" => $utilisateur->getNom(),
            "prenomTag" => $utilisateur->getPrenom(),
        );
    }



}
