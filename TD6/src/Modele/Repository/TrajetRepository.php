<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;
use App\Covoiturage\Modele\DataObject\Trajet;
use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees; // Assurez-vous que c'est le bon chemin
use App\Covoiturage\Modele\Repository\UtilisateurRepository; // Ajoutez cette ligne pour UtilisateurRepository

class TrajetRepository extends AbstractRepository {

    protected function getNomTable(): string
    {
        return "trajet";
    }

    protected function formatTableauSQL(AbstractDataObject $trajet): array {
    /** @var Trajet $trajet */
    return array(
        "departTag" => $trajet->getDepart(),
        "arriveTag" => $trajet->getArrive(),
        "dateTag" => $trajet->getDate(),
        "prixTag" => $trajet->getPrix(),
        "conducteurLoginTag" => $trajet->getConducteurLogin(),
        "nonFumeurTag" => $trajet->getNonFumeur() ? 1 : 0 // Assurez-vous de retourner un entier
    );
}




    protected function getNomsColonnes(): array {
        return ['depart', 'arrive', 'date', 'prix', 'conducteurLogin', 'nonFumeur'];
    }



    protected function getNomClePrimaire(): string {
        return 'id'; // Clé primaire de la table 'trajet'
    }

    

    public static function recupererPassagers(Trajet $trajet): array {
        $sql = "SELECT utilisateur_login FROM passagers WHERE trajet_id = :id";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute(['id' => $trajet->getId()]);
        $passagersTableau = $pdoStatement->fetchAll();

        $passagers = [];
        foreach ($passagersTableau as $passagerTableau) {
            $passagers[] = UtilisateurRepository::recupererUtilisateurParLogin($passagerTableau['utilisateur_login']);
        }
        return $passagers;
    }

    

    public static function ajouterTrajet(Trajet $trajet): void {
        $sql = "INSERT INTO trajet (depart, arrive, date, prix, conducteurLogin, nonFumeur) VALUES (:depart, :arrive, :date, :prix, :conducteurLogin, :nonFumeur)"; // Renommé de trajets à trajet
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute([
            'depart' => $trajet->getDepart(),
            'arrive' => $trajet->getArrive(),
            'date' => $trajet->getDate(),
            'prix' => $trajet->getPrix(),
            'conducteurLogin' => $trajet->getConducteurLogin(),
            'nonFumeur' => $trajet->isNonFumeur(),
        ]);
    }

    public static function mettreAJourTrajet(Trajet $trajet): void {
        $sql = "UPDATE trajet SET depart = :depart, arrive = :arrive, date = :date, prix = :prix, conducteurLogin = :conducteurLogin, nonFumeur = :nonFumeur WHERE id = :id"; // Renommé de trajets à trajet
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute([
            'depart' => $trajet->getDepart(),
            'arrive' => $trajet->getArrive(),
            'date' => $trajet->getDate(),
            'prix' => $trajet->getPrix(),
            'conducteurLogin' => $trajet->getConducteurLogin(),
            'nonFumeur' => $trajet->isNonFumeur(),
            'id' => $trajet->getId(),
        ]);
    }

    public static function supprimerTrajet(int $id): void {
        $sql = "DELETE FROM trajet WHERE id = :id"; // Renommé de trajets à trajet
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute(['id' => $id]);
    }

    

    

    protected function construireDepuisTableauSQL(array $trajetTableau): Trajet {
        return new Trajet(
            $trajetTableau['id'],
            $trajetTableau['depart'],
            $trajetTableau['arrive'],
            $trajetTableau['date'],
            $trajetTableau['prix'],
            $trajetTableau['conducteurLogin'],
            $trajetTableau['nonFumeur']
        );
    }
}
