<?php

namespace App\Covoiturage\Modele\Repository;

use App\Covoiturage\Modele\Repository\ConnexionBaseDeDonnees;
use App\Covoiturage\Modele\DataObject\Utilisateur ;
use App\Covoiturage\Modele\DataObject\AbstractDataObject;

abstract class AbstractRepository {

    protected abstract function getNomTable(): string;

    /** @return string[] */
    protected abstract function getNomsColonnes(): array;


    protected abstract function getNomClePrimaire(): string;

    protected abstract function formatTableauSQL(AbstractDataObject $objet): array;


    /**
     * @return AbstractDataObject
     */
    public  function recupererParClePrimaire(string $clePrimaire): ?AbstractDataObject {
        $sql = " SELECT * FROM " . $this::getNomTable() . " WHERE " . $this::getNomClePrimaire() . " = :cleTag";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute(['cleTag' => $clePrimaire]);
    
        $objetFormatTableau = $pdoStatement->fetch();
    
        if ($objetFormatTableau === false) {
            return null;
        }
    
        
        return $this->construireDepuisTableauSQL($objetFormatTableau);
    }

    /**
     * @return AbstractDataObject[]
     */
    public function recuperer(): array {

        $sql = "SELECT * FROM " .$this::getNomTable();

        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query($sql);
        $utilisateurs = [];

        while ($utilisateurFormatTableau = $pdoStatement->fetch()) {
            $utilisateurs[] = $this::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }

    public function supprimer($valeurClePrimaire): void {
        $sql = "DELETE FROM " . $this->getNomTable() . " WHERE " . $this->getNomClePrimaire() . " = :clePrimaire";
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        $pdoStatement->execute(['clePrimaire' => $valeurClePrimaire]);
    }

    public function ajouter(AbstractDataObject $objet): bool {
        $nomsColonnes = $this->getNomsColonnes(); // ["depart", "arrivee", "date", "prix", "conducteurLogin", "nonFumeur"]

        // Construit la chaîne de colonnes et celle des tags pour les paramètres
        $colonnes = join(', ', $nomsColonnes);
        $tags = join(', ', array_map(fn($colonne) => ':' . $colonne . 'Tag', $nomsColonnes));

        // Construction de la requête SQL
        $sql = "INSERT INTO " . $this->getNomTable() . " ($colonnes) VALUES ($tags)";

        // Log de la requête pour débogage (optionnel)
        // echo $sql;

        // Formatage des données pour l'exécution
        $valeurs = $this->formatTableauSQL($objet); // Assurez-vous que cela correspond bien aux colonnes attendues

        // Préparation et exécution de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);
        return $pdoStatement->execute($valeurs);
    }

    public function mettreAJour(AbstractDataObject $objet): void {
        // Construire la requête SQL
        $setClause = [];
        foreach ($this->getNomsColonnes() as $colonne) {
            $setClause[] = "$colonne = :$colonne";
        }
        $setClauseString = implode(', ', $setClause);
        $nomTable = $this->getNomTable();
        $nomClePrimaire = $this->getNomClePrimaire();

        $sql = "UPDATE $nomTable SET $setClauseString WHERE $nomClePrimaire = :clePrimaireTag";

        // Afficher la requête SQL pour débogage
        echo "Requête SQL : $sql\n";

        $stmt = $this->pdo->prepare($sql);
        $tableauSQL = $this->formatTableauSQL($objet);

        // Assurez-vous d'inclure la clé primaire dans le tableau
        $tableauSQL['clePrimaireTag'] = $objet->getLogin(); // Remplacez getLogin() par la méthode appropriée pour obtenir la clé primaire

        $stmt->execute($tableauSQL);
    }



    protected abstract function construireDepuisTableauSQL(array $objetFormatTableau) : AbstractDataObject;

    
    

}