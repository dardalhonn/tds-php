<?php
namespace App\Covoiturage\Modele\DataObject;

use App\Covoiturage\Modele\DataObject\AbstractDataObject;

class Trajet extends AbstractDataObject {
    private ?int $id;
    private string $depart;
    private string $arrive;
    private string $date;
    private float $prix;
    private string $conducteurLogin;
    private bool $nonFumeur;

    public function __construct($id, $depart, $arrive, $date, $prix, $conducteurLogin, $nonFumeur) {
        $this->id = $id;
        $this->depart = $depart;
        $this->arrive = $arrive;
        $this->date = $date;
        $this->prix = $prix;
        $this->conducteurLogin = $conducteurLogin;
        $this->nonFumeur = $nonFumeur;
    }

    public function getId() {
        return $this->id;
    }

    public function getDepart() {
        return $this->depart;
    }

    public function getArrive() {
        return $this->arrive;
    }

    public function getDate() {
        return $this->date;
    }

    public function getPrix() {
        return $this->prix;
    }

    public function getConducteurLogin() {
        return $this->conducteurLogin;
    }

    public function getNonFumeur() {
        return $this->nonFumeur;
    }

    public function setDepart($depart) {
        $this->depart = $depart;
    }

    public function setArrive($arrive) {
        $this->arrive = $arrive;
    }

    public function setDate($date) {
        $this->date = $date;
    }

    public function setPrix($prix) {
        $this->prix = $prix;
    }

    public function setConducteurLogin($conducteurLogin) {
        $this->conducteurLogin = $conducteurLogin;
    }

    public function setNonFumeur($nonFumeur) {
        $this->nonFumeur = $nonFumeur;
    }
}
