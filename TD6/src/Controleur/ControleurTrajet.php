<?php
namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\Repository\TrajetRepository; // Ajoutez cette ligne si elle manque
use App\Covoiturage\Modele\DataObject\Trajet;

class ControleurTrajet
{

    public static function afficherListe()
    {
        $trajets = (new TrajetRepository())->recuperer();
        $titre = 'Liste des trajets';
        $cheminCorpsVue = 'trajet/liste.php';
        require __DIR__ . '/../vue/vueGenerale.php';
    }

    public static function afficherVue(string $cheminVue, array $parametres = [])
    {
        $titre = $parametres['titre'] ?? 'Trajet';
        $cheminCorpsVue = $cheminVue;
        extract($parametres);
        require __DIR__ . '/../vue/vueGenerale.php';
    }

    public static function afficherErreur(string $message)
    {
        $titre = 'Erreur';
        $cheminCorpsVue = 'trajet/erreur.php';
        require __DIR__ . '/../vue/vueGenerale.php';
    }

    public static function afficherDetail(): void
    {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
            $trajet = (new TrajetRepository())->recupererParClePrimaire($id);

            if ($trajet === null) {
                echo "Le trajet avec l'ID $id n'existe pas.";
            } else {
                require_once __DIR__ . '/../vue/trajet/detail.php';
            }
        } else {
            echo "ID non spécifié.";
        }
    }

    public static function supprimer()
    {
        $id = $_GET['id'];

        $trajetRepository = new TrajetRepository();
        $trajetRepository->supprimer($id);  // Utilisation de la méthode générique de suppression

        require_once __DIR__ . '/../vue/trajet/trajetSupprime.php';
    }

    public static function afficherFormulaireCreation(): void
    {
        // Affiche la vue du formulaire de création de trajets
        require_once __DIR__ . '/../vue/trajet/formulaireCreation.php';
    }

    public static function creerDepuisFormulaire() {
        $depart = $_GET['depart'] ?? null;
        $arrive = $_GET['arrivee'] ?? null;
        $date = $_GET['date'] ?? null;
        $prix = $_GET['prix'] ?? null;
        $conducteurLogin = $_GET['conducteurLogin'] ?? null;

        if ($depart && $arrive && $date && $prix && $conducteurLogin) {
            $trajet = new Trajet(null, $depart, $arrive, $date, $prix, $conducteurLogin, false);
            (new TrajetRepository())->ajouter($trajet);
            require_once __DIR__ ."/../vue/trajet/trajetCree.php";
        } else {
            // Gérez le cas où certaines données sont manquantes
            echo "Erreur: données manquantes.";
        }
    }

    public static function afficherFormulaireMiseAJour(): void {
        if (isset($_GET['id'])) {
            $id = $_GET['id'];
        // Récupérez le trajet à partir de l'identifiant
        $trajet = (new TrajetRepository())->recupererParClePrimaire($id);

        // Vérifiez que le trajet existe
        if ($trajet) {
            // Inclure la vue avec le trajet à mettre à jour
            require __DIR__ . '/../vue/trajet/formulaireMiseAJour.php';
        } else {
            // Gérer le cas où le trajet n'existe pas
            self::afficherErreur("Le trajet avec l'identifiant '$id' n'existe pas.");
        }
    }
}

public static function mettreAJour(): void {
    // Vérifiez que les données POST sont présentes avant de procéder
    if (isset($_POST['depart'], $_POST['arrivee'], $_POST['date'], $_POST['prix'], $_POST['conducteurLogin'])) {
        // Récupérer l'utilisateur à partir des données du formulaire
        $trajet = new Trajet(
            $_POST['depart'], $_POST['arrivee'], $_POST['date'], $_POST['prix'], $_POST['conducteurLogin']
        );

        // Créer une instance de UtilisateurRepository
        $trajetRepository = new TrajetRepository();

        // Mettre à jour l'utilisateur dans le référentiel
        $trajetRepository->mettreAJour($trajet);

        // Inclure la vue après la mise à jour
        require __DIR__ . '/../vue/trajet/liste.php';
    } 
}
}
