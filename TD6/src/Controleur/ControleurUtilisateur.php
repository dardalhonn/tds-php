<?php



namespace App\Covoiturage\Controleur;

use App\Covoiturage\Modele\DataObject\Utilisateur;
use App\Covoiturage\Modele\Repository\UtilisateurRepository;


class ControleurUtilisateur
{


    public static function afficherListe() {
        $utilisateurs = (new UtilisateurRepository())->recuperer();
        $titre = "Liste des utilisateurs";
        $cheminCorpsVue = "utilisateur/liste.php";
        require __DIR__ . '/../vue/vueGenerale.php';
    }

    public static function afficherErreur(string $message): void {
        echo "<h1>Problème avec l'utilisateur</h1>";
        echo "<p>$message</p>";
    }
    /** @var Utilisateur[] $utilisateurs */
    public static function supprimer():void  {
        if (!isset($_GET['login']) || empty($_GET['login'])) {
            ControleurUtilisateur::afficherErreur("Message d'erreur spécifique");
            return;
        }
        $login = $_GET['login'];
        UtilisateurRepository::supprimerParLogin($login);

        /** @var Utilisateur[] $utilisateurs */
        $utilisateurs = (new UtilisateurRepository())->recuperer();

        $titre = "Utilisateur supprimé"; // Titre de la page
        $cheminCorpsVue = "utilisateur/utilisateurSupprime.php"; // Chemin vers votre nouvelle vue

        // Passez la liste d'utilisateurs à la vue générale
        require __DIR__ . '/../vue/vueGenerale.php'; // Cela inclura également la variable $utilisateurs

    }

    public static function afficherFormulaireMiseAJour(): void {
        // Vérifiez si le login est passé dans l'URL
        if (isset($_GET['login'])) {
            $login = $_GET['login'];

            // Récupérez l'utilisateur par login
            $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

            // Vérifiez si l'utilisateur existe
            if ($utilisateur !== null) {
                // Affichez la vue du formulaire de mise à jour
                require __DIR__ . '/../vue/utilisateur/formulaireMiseAJour.php';
            } else {
                // Affichez une vue d'erreur si l'utilisateur n'existe pas
                $messageErreur = "L'utilisateur avec le login " . htmlspecialchars($login) . " n'existe pas.";
                require __DIR__ . '/../vue/utilisateur/erreur.php';
            }
        } else {
            // Affichez une vue d'erreur si le login n'est pas spécifié
            $messageErreur = "Aucun login n'a été fourni pour la mise à jour.";
            require __DIR__ . '/../vue/utilisateur/erreur.php';
        }
    }

    public static function mettreAJour(): void {
        // Vérifiez que les données POST sont présentes avant de procéder
        if (isset($_POST['login'], $_POST['nom'], $_POST['prenom'])) {
            // Récupérer l'utilisateur à partir des données du formulaire
            $utilisateur = new Utilisateur(
                $_POST['login'],
                $_POST['nom'],
                $_POST['prenom']
            );

            // Créer une instance de UtilisateurRepository
            $utilisateurRepository = new UtilisateurRepository();

            // Mettre à jour l'utilisateur dans le référentiel
            $utilisateurRepository->mettreAJour($utilisateur);

            // Inclure la vue après la mise à jour
            require __DIR__ . '/../vue/utilisateur/liste.php';
        } else {
            // Gérer le cas où les données sont manquantes
            self::afficherErreur("Données du formulaire manquantes.");
        }


}




    public static function creerDepuisFormulaire(): void {
        // Vérification que les clés existent dans le tableau $_POST
        if (isset($_GET['login']) && isset($_GET['nom']) && isset($_GET['prenom'])) {
            // Récupération des valeurs du formulaire
            $login = $_GET['login'];
            $nom = $_GET['nom'];
            $prenom = $_GET['prenom'];

            // Création de l'objet Utilisateur
            $utilisateur = new Utilisateur($login, $nom, $prenom);

            // Ajout de l'utilisateur en base de données
            (new UtilisateurRepository())->ajouter($utilisateur);

            // Redirection ou autre action après création
            // Exemple de redirection
            require __DIR__ . '/../vue/utilisateur/utilisateurCree.php';
            exit();
        } else {
            // Si des champs sont manquants
            echo "Veuillez remplir tous les champs du formulaire.";
        }
    }

    public static function afficherDetail(): void
    {
        if (!isset($_GET['login']) || empty($_GET['login'])) {
            ControleurUtilisateur::afficherErreur("Message d'erreur spécifique");
            return;
        }

        $login = $_GET['login'];
        $utilisateur = (new UtilisateurRepository())->recupererParClePrimaire($login);

        if (empty($utilisateur)) {
            ControleurUtilisateur::afficherErreur("Message d'erreur spécifique");
        } else {
            self::afficherVue('vueGenerale.php', [
                'titre' => 'Détails de l\'utilisateur',
                'cheminCorpsVue' => 'utilisateur/detail.php',
                'utilisateurEnParametre' => $utilisateur,
            ]);
        }
    }


    public static function afficherFormulaireCreation(): void
    {
        // Afficher la vue du formulaire de création
        self::afficherVue('utilisateur/formulaireCreation.php');
    }
    /** @var Utilisateur[] $utilisateurs */







    // Méthode privée pour afficher les vues
    private static function afficherVue(string $cheminVue, array $parametres = []): void
    {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require __DIR__ . '/../vue/' . $cheminVue; // Utiliser un chemin absolu pour la vue
    }
}
