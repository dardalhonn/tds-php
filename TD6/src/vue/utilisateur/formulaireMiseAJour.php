<!-- src/vue/utilisateur/formulaireMiseAJour.php -->

<form action="controleurFrontal.php" method="post">
    <input type="hidden" name="action" value="mettreAJour">

    <label for="login">Login :</label>
    <input type="text" id="login" name="login" value="<?= htmlspecialchars($utilisateur->getLogin()) ?>" readonly><br>

    <label for="nom">Nom :</label>
    <input type="text" id="nom" name="nom" value="<?= htmlspecialchars($utilisateur->getNom()) ?>"><br>

    <label for="prenom">Prénom :</label>
    <input type="text" id="prenom" name="prenom" value="<?= htmlspecialchars($utilisateur->getPrenom()) ?>"><br>

    <button type="submit">Mettre à jour</button>
</form>
