<!-- src/vue/utilisateur/liste.php -->

<table>
    <tr>
        <th>Login</th>
        <th>Actions</th>
    </tr>
    <?php foreach ($utilisateurs as $utilisateur): ?>
        <tr>
            <td><a href="controleurFrontal.php?controleur=utilisateur&action=afficherDetail&login=<?= urlencode($utilisateur->getLogin()) ?>">
                    <?= htmlspecialchars($utilisateur->getLogin()) ?>
                </a></td>
            <td>
                <!-- Lien de suppression -->
                <a href="controleurFrontal.php?controleur=utilisateur&action=supprimer&login=<?= urlencode($utilisateur->getLogin()) ?>">Supprimer</a>
                <!-- Lien de mise à jour -->
                <a href="controleurFrontal.php?controleur=utilisateur&action=afficherFormulaireMiseAJour&login=<?= urlencode($utilisateur->getLogin()) ?>">Mettre à jour</a>
            </td>
        </tr>
    <?php endforeach; ?>
</table>
