<!-- src/vue/utilisateur/utilisateurMisAJour.php -->

<h1>L'utilisateur a été mis à jour</h1>
<p>L'utilisateur de login <?= htmlspecialchars($login) ?> a bien été mis à jour.</p>

<h2>Liste des utilisateurs :</h2>

<!-- Inclusion de la liste des utilisateurs -->
<?php require __DIR__ . '/liste.php'; ?>
