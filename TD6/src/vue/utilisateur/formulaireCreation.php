<!-- TD5/vue/utilisateur/formulaireCreation.php -->
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Création d'un utilisateur</title>
    <link rel="stylesheet" href="ressources/css/style.css"> <!-- Ajoutez ici si nécessaire -->
</head>
<body>
<h2>Créer un utilisateur</h2>
<form action="controleurFrontal.php" method="GET">
    <input type="hidden" name="action" value="creerDepuisFormulaire">

    <p class="InputAddOn">
        <label class="InputAddOn-item" for="login_id">Login&#42;</label>
        <input class="InputAddOn-field" type="text" placeholder="Ex : leblancj" name="login" id="login_id" required>
    </p>

    <p class="InputAddOn">
        <label class="InputAddOn-item" for="nom_id">Nom&#42;</label>
        <input class="InputAddOn-field" type="text" placeholder="Ex : Leblanc" name="nom" id="nom_id" required>
    </p>

    <p class="InputAddOn">
        <label class="InputAddOn-item" for="prenom_id">Prénom&#42;</label>
        <input class="InputAddOn-field" type="text" placeholder="Ex : Jean" name="prenom" id="prenom_id" required>
    </p>

    <input type="submit" value="Créer l'utilisateur">
</form>
</body>
</html>
