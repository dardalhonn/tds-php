<!-- src/Vue/trajet/trajetCree.php -->

<h1>Trajet créé avec succès</h1>

<p>Le trajet de <?= htmlspecialchars($depart) ?> à <?= htmlspecialchars($arrivee) ?> pour le <?= htmlspecialchars($date) ?> a été ajouté avec succès.</p>

<a href="controleurFrontal.php?controleur=trajet&action=afficherListe">Voir la liste des trajets</a>
