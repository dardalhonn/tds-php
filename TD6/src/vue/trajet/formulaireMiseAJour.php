<form action="controleurFrontal.php?controleur=trajet" method="POST">
    <input type="hidden" name="action" value="mettreAJour"> <!-- Action adaptée pour la création -->

    <label for="depart">Départ :</label>
    <input type="text" id="depart" name="depart" value="<?= isset($trajet) ? htmlspecialchars($trajet->getDepart()) : '' ?>" required><br>

    <label for="arrive">Arrivée :</label>
    <input type="text" id="arrive" name="arrivee" value="<?= isset($trajet) ? htmlspecialchars($trajet->getArrive()) : '' ?>" required><br>

    <label for="date">Date :</label>
    <input type="date" id="date" name="date" value="<?= isset($trajet) ? htmlspecialchars($trajet->getDate()) : '' ?>" required><br>

    <label for="prix">Prix :</label>
    <input type="number" id="prix" name="prix" value="<?= isset($trajet) ? htmlspecialchars($trajet->getPrix()) : '' ?>" required><br>

    <label for="conducteurLogin">Conducteur :</label>
    <input type="text" id="conducteurLogin" name="conducteurLogin" value="<?= isset($trajet) ? htmlspecialchars($trajet->getConducteurLogin()) : '' ?>" required><br>

    <label for="nonFumeur">Non Fumeur :</label>
    <input type="checkbox" id="nonFumeur" name="nonFumeur" <?= isset($trajet) && $trajet->getNonFumeur() ? 'checked' : '' ?>><br>

    <button type="submit">Créer</button>
</form>
