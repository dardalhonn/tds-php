<!-- Vue/trajet/detail.php -->

<h1>Détail du trajet</h1>

<p><strong>ID : </strong><?= htmlspecialchars($trajet->getId()) ?></p>
<p><strong>départ : </strong><?= htmlspecialchars($trajet->getDepart()) ?></p>
<p><strong>arrivé : </strong><?= htmlspecialchars($trajet->getArrive()) ?></p>
<p><strong>Date : </strong><?= htmlspecialchars($trajet->getDate()) ?></p>
<p><strong>prix : </strong><?= htmlspecialchars($trajet->getPrix()) ?></p>

<a href="controleurFrontal.php?controleur=trajet&action=afficherListe">Retour à la liste des trajets</a>