<!-- src/vue/trajet/liste.php -->

<h1>Liste des trajets</h1>

<table>
    <tr>
        <th>ID</th>
        <th>Départ</th>
        <th>Arrivée</th>
        <th>Date</th>
        <th>Prix</th>
        <th>Conducteur</th>
        <th>Non-Fumeur</th>
        <th>Actions</th> <!-- Nouvelle colonne pour les actions -->
    </tr>
    <a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireCreation">Créer un nouveau trajet</a>

    <?php foreach ($trajets as $trajet): ?>
        <tr>
            <td><?= htmlspecialchars($trajet->getId()) ?></td>
            <td><?= htmlspecialchars($trajet->getDepart()) ?></td>
            <td><?= htmlspecialchars($trajet->getArrive()) ?></td>
            <td><?= htmlspecialchars($trajet->getDate()) ?></td>
            <td><?= htmlspecialchars($trajet->getPrix()) ?></td>
            <td><?= htmlspecialchars($trajet->getConducteurLogin()) ?></td>
            <td><?= htmlspecialchars($trajet->getNonFumeur() ? 'Oui' : 'Non') ?></td>
            <td>
                <!-- Lien pour voir les détails du trajet -->
                <a href="controleurFrontal.php?controleur=trajet&action=afficherDetail&id=<?= urlencode($trajet->getId()) ?>">Voir détail</a>
                <a href="controleurFrontal.php?controleur=trajet&action=afficherFormulaireMiseAJour&id=<?= urlencode($trajet->getId()) ?>">Mettre à jour</a>
                <!-- Lien pour supprimer le trajet avec confirmation -->
                <a href="controleurFrontal.php?controleur=trajet&action=supprimer&id=<?= urlencode($trajet->getId()) ?>"
                   onclick="return confirm('Êtes-vous sûr de vouloir supprimer ce trajet ?');">
                    Supprimer
                </a>

            </td>
        </tr>
    <?php endforeach; ?>
</table>
