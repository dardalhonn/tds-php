<!-- src/vue/trajet/formulaireCreation.php -->

<h1>Créer un nouveau trajet</h1>

<form action="controleurFrontal.php" method="GET">
    <!-- Champs cachés pour spécifier le contrôleur et l'action -->
    <input type="hidden" name="controleur" value="trajet">
    <input type="hidden" name="action" value="creerDepuisFormulaire">

    <!-- Champ pour le départ -->
    <label for="depart">Départ:</label>
    <input type="text" id="depart" name="depart" required>
    <br>

    <!-- Champ pour l'arrivée -->
    <label for="arrivee">Arrivée:</label>
    <input type="text" id="arrivee" name="arrivee" required>
    <br>

    <!-- Champ pour la date -->
    <label for="date">Date:</label>
    <input type="date" id="date" name="date" required>
    <br>

    <!-- Champ pour le prix -->
    <label for="prix">Prix:</label>
    <input type="number" id="prix" name="prix" required>
    <br>

    <!-- Champ pour le conducteur -->
    <label for="conducteurLogin">Conducteur Login:</label>
    <input type="text" id="conducteurLogin" name="conducteurLogin" required>
    <br>

    <!-- Bouton pour soumettre le formulaire -->
    <input type="submit" value="Créer le trajet">
</form>
