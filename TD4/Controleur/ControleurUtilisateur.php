<?php

use Modele\ModeleUtilisateur;

require_once ('../Modele/ModeleUtilisateur.php'); // chargement du modèle

class ControleurUtilisateur {
    // Déclaration de type de retour void : la fonction ne retourne pas de valeur
    public static function afficherListe() : void {
        $utilisateurs = ModeleUtilisateur::recupererUtilisateurs(); // appel au modèle pour gérer la BD
        // Utilisation de afficherVue() pour afficher la vue
        self::afficherVue('utilisateur/liste.php', ['utilisateurs' => $utilisateurs]);
    }

    public static function afficherDetail() : void {
        if (!isset($_GET['login']) || empty($_GET['login'])) {
            echo "Login non fourni";
            self::afficherVue('utilisateur/erreur.php');
            return;
        }

        $login = $_GET['login'];

        $utilisateur = ModeleUtilisateur::recupererUtilisateurParLogin($login);

        if (empty($utilisateur)) {
            self::afficherVue('utilisateur/erreur.php');
        } else {
            self::afficherVue('utilisateur/detail.php', ['utilisateurEnParametre' => $utilisateur]);
        }
    }

    public static function afficherFormulaireCreation() : void {
        // Afficher la vue du formulaire de création
        self::afficherVue('utilisateur/formulaireCreation.php');
    }

    public static function creerDepuisFormulaire() : void {
        // Vérifier si les champs requis sont fournis dans l'URL (GET)
        if (!isset($_GET['login'], $_GET['nom'], $_GET['prenom'])) {
            self::afficherVue('utilisateur/erreur.php', ['message' => 'Tous les champs sont requis.']);
            return;
        }

        // Récupérer les données du formulaire
        $login = $_GET['login'];
        $nom = $_GET['nom'];
        $prenom = $_GET['prenom'];


        $utilisateur = new ModeleUtilisateur($login, $nom, $prenom);
        $utilisateur->ajouter();


        // Vérifier si l'insertion en base a réussi
        if ($utilisateur) {
            // Rediriger vers la liste des utilisateurs
            self::afficherListe();
        } else {
            self::afficherVue('utilisateur/erreur.php', ['message' => 'Erreur lors de la création de l\'utilisateur.']);
        }
    }



    // Méthode privée pour afficher les vues
    private static function afficherVue(string $cheminVue, array $parametres = []) : void {
        extract($parametres); // Crée des variables à partir du tableau $parametres
        require "../vue/$cheminVue"; // Charge la vue
    }
}
