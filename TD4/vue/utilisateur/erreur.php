<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Erreur - Utilisateur</title>
</head>
<body>
<h1>Problème avec l'utilisateur</h1>
<p>Nous avons rencontré un problème en essayant de récupérer les informations de l'utilisateur. Veuillez vérifier les informations fournies.</p>

<a href="http://localhost/tds-php/TD4/Controleur/routeur.php?action=afficherListe">Retour à la liste des utilisateurs</a>
</body>
</html>
