<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Création d'un utilisateur</title>
</head>
<body>
<h2>Créer un utilisateur</h2>
<form action="routeur.php" method="GET">
    <input type="hidden" name="action" value="creerDepuisFormulaire">

    <label for="login">Login:</label>
    <input type="text" id="login" name="login" required><br><br>

    <label for="nom">Nom:</label>
    <input type="text" id="nom" name="nom" required><br><br>

    <label for="prenom">Prénom:</label>
    <input type="text" id="prenom" name="prenom" required><br><br>


    <input type="submit" value="Créer l'utilisateur">
</form>
</body>
</html>
