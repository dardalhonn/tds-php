<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Liste de utilisateurs</title>
</head>
<body>
<?php
if (isset($utilisateurEnParametre)) :
    echo "Nom : " . $utilisateurEnParametre->getNom() . "<br>";
    echo "Prénom : " . $utilisateurEnParametre->getPrenom() . "<br>";
    echo "Login : " . $utilisateurEnParametre->getLogin();
else:
    echo "Utilisateur introuvable.";
endif;
?>

</body>
</html>
