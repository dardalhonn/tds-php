<?php


use Modele\ConnexionBaseDeDonnees;

require_once '../Configuration/ConfigurationBaseDeDonnees.php';
require_once 'ConnexionBaseDeDonnees.php';


class ModeleUtilisateur
{

    // Déclaration des attributs de classe comme string
    private string $login;
    private string $nom;
    private string $prenom;

    // Constructeur avec les types des arguments
    public function __construct(string $login, string $nom, string $prenom)
    {
        $this->login = substr($login, 0, 64);  // Limite à 64 caractères
        $this->nom = $nom;
        $this->prenom = $prenom;
    }

    // Getter pour le nom (retourne une chaîne de caractères)
    public function getNom(): string
    {
        return $this->nom;
    }

    // Setter pour le nom (prend une chaîne de caractères en paramètre)
    public function setNom(string $nom): void
    {
        $this->nom = $nom;
    }

    // Getter pour le prénom (retourne une chaîne de caractères)
    public function getPrenom(): string
    {
        return $this->prenom;
    }

    // Setter pour le prénom (prend une chaîne de caractères en paramètre)
    public function setPrenom(string $prenom): void
    {
        $this->prenom = $prenom;
    }

    // Getter pour le login (retourne une chaîne de caractères)
    public function getLogin(): string
    {
        return $this->login;
    }

    // Setter pour le login avec limitation à 64 caractères (prend une chaîne de caractères en paramètre)
    public function setLogin(string $login): void
    {
        $this->login = substr($login, 0, 64);
    }


   /* public function __toString(): string
    {
        return "Utilisateur {$this->prenom} {$this->nom} de login {$this->login}";
    }
   */

    public static function construireDepuisTableauSQL(array $utilisateurFormatTableau): Modele\ModeleUtilisateur
    {
        return new Modele\ModeleUtilisateur(
            $utilisateurFormatTableau['login'] ?? '',
            $utilisateurFormatTableau['nom'] ?? '',
            $utilisateurFormatTableau['prenom'] ?? ''
        );
    }

    public static function recupererUtilisateurs(): array
    {
        $sql = "SELECT * FROM utilisateur";


        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->query($sql);


        $utilisateurs = [];
        while ($utilisateurFormatTableau = $pdoStatement->fetch(PDO::FETCH_ASSOC)) {
            $utilisateurs[] = Modele\ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
        }

        return $utilisateurs;
    }




    public static function recupererUtilisateurParLogin(string $login) : ?Modele\ModeleUtilisateur {
        $sql = "SELECT * from utilisateur WHERE login = :loginTag";
        // Préparation de la requête
        $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

        $values = array(
            "loginTag" => $login,

        );

        $pdoStatement->execute($values);


        $utilisateurFormatTableau = $pdoStatement->fetch();
        if ($utilisateurFormatTableau === false) {
            return null;
        }

        return Modele\ModeleUtilisateur::construireDepuisTableauSQL($utilisateurFormatTableau);
    }




    public function ajouter() : void {
        try {
            // Utilisation de placeholders avec une requête préparée
            $sql = "INSERT INTO utilisateur (login, nom, prenom) VALUES (:login, :nom, :prenom)";

            // Préparation de la requête
            $pdoStatement = ConnexionBaseDeDonnees::getPdo()->prepare($sql);

            // Exécution avec un tableau de valeurs associatives
            $pdoStatement->execute([
                ':login' => $this->login,
                ':nom' => $this->nom,
                ':prenom' => $this->prenom
            ]);

            echo "L'utilisateur {$this->login} a été ajouté avec succès.<br>";
        } catch (PDOException $e) {
            // Affichage du message d'erreur en cas d'exception
            echo "Erreur lors de l'insertion de l'utilisateur : " . $e->getMessage() . "<br>";
        }
    }


}
